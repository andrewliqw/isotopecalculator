#include <iostream>

//#include <boost/lexical_cast.hpp>
#include <globalvar.hh>


#include "envelopedialog.hh"

EnvelopeDialog::EnvelopeDialog()
  : leftLabel( "Left level" ),
    rightLabel( "Right level" ),
    leftAdjustment( 0, 0, 10, 1, 1, 0 ),
    rightAdjustment( 0, 0, 10, 1, 1, 0 ),
    leftLevel( leftAdjustment ),
    rightLevel( rightAdjustment ),
    lowerLabel( "Lower bound" ),
    upperLabel( "Upper bound" ),
    stepLabel( "Step" ),
    rLabel( "Resolution" ),
    normLabel( "Normalization"),
    lowerEntry( "^\\d*\\.?\\d*$" ),
    upperEntry( "^\\d*\\.?\\d*$" ),
    stepEntry( "^\\d*\\.?\\d*$" ),
    rEntry( "^\\d*\\.?\\d*$" ),
    normEntry( "^\\d*\\.?\\d*$" ),
    gaussianButton( "Gaussian" ),
    cauchyLorentzButton( "Cauchy-Lorentz" ),
    inverseCauchyLorentzButton( "Inverse Cauchy-Lorentz" )
{
  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nEntering EnvelopeDialog::EnvelopeDialog ..." << std::endl;
  }

  set_title("Set the parameters for envelope" );
  Gtk::VBox *box = get_vbox();

  leftBox.pack_start( leftLabel );
  leftBox.pack_start( leftLevel );
  leftBox.pack_start( rLabel );
  leftBox.pack_start( rEntry );

  rightBox.pack_start( rightLabel );
  rightBox.pack_start( rightLevel );
  rightBox.pack_start( normLabel );
  rightBox.pack_start( normEntry );

  hbox.pack_start( leftBox );
  hbox.pack_start( rightBox );
  box->pack_start( hbox );

  hbox2.pack_start( lowerLabel );
  hbox2.pack_start( lowerEntry );
  hbox2.pack_start( upperLabel );
  hbox2.pack_start( upperEntry );
  hbox2.pack_start( stepLabel );
  hbox2.pack_start( stepEntry );
  box->pack_start( hbox2 );

  //hbox3.pack_start( rLabel );
  //hbox3.pack_start( rEntry );
  //hbox3.pack_start( normLabel );
  //hbox3.pack_start( normEntry );
  //box->pack_start( hbox3 );

  Gtk::RadioButton::Group group = gaussianButton.get_group();
  cauchyLorentzButton.set_group( group );
  inverseCauchyLorentzButton.set_group( group );
  gaussianButton.set_active();
  inverseCauchyLorentzButton.set_sensitive( false );

  box->pack_start( buttonBox, Gtk::PACK_SHRINK );
  buttonBox.pack_start( gaussianButton, Gtk::PACK_SHRINK );
  buttonBox.pack_start( cauchyLorentzButton, Gtk::PACK_SHRINK );
  //buttonBox.pack_start( inverseCauchyLorentzButton, Gtk::PACK_SHRINK );

  leftLevel.signal_value_changed().connect
    ( sigc::mem_fun( *this, &EnvelopeDialog::setRightLevelMin ) );

  add_button( Gtk::Stock::OK, Gtk::RESPONSE_OK );
  add_button( Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );

  show_all_children();

  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nLeaving EnvelopeDialog::EnvelopeDialog ..." << std::endl;
  }
}

EnvelopeDialog::~EnvelopeDialog()
{}

// constructor and setMaxLevel functions guarantee
// left level always greater than or equal to zero
unsigned int EnvelopeDialog::getLeftLevel()
{
  return static_cast<unsigned int>( leftLevel.get_value() );
}

// constructor and setMaxLevel functions guarantee
// right level always greater than or equal to zero
unsigned int EnvelopeDialog::getRightLevel()
{
  return static_cast<unsigned int>( rightLevel.get_value() );
}

double EnvelopeDialog::getLower()
{
  std::string str( lowerEntry.get_text() );
  std::istringstream iss( str );
  double x;
  iss >> x;
  return x;
  //return boost::lexical_cast<double>( str );
}

double EnvelopeDialog::getUpper()
{
  std::string str( upperEntry.get_text() );
  std::istringstream iss( str );
  double x;
  iss >> x;
  return x;
  //return boost::lexical_cast<double>( str );
}

double EnvelopeDialog::getStep()
{
  std::string str( stepEntry.get_text() );
  std::istringstream iss( str );
  double x;
  iss >> x;
  return x;
  //return boost::lexical_cast<double>( str );
}

double EnvelopeDialog::getResolution()
{
  std::string str( rEntry.get_text() );
  std::istringstream iss( str );
  double x;
  iss >> x;
  return x;
  //return boost::lexical_cast<double>( str );
}

double EnvelopeDialog::getNormalization()
{
  std::string str( normEntry.get_text() );
  std::istringstream iss( str );
  double x;
  iss >> x;
  return x;
  //return boost::lexical_cast<double>( str );
}

TheoreticalEnvelope::ConvolutionFunctionType EnvelopeDialog::getType()
{
  if( gaussianButton.get_active() )
    return TheoreticalEnvelope::Gaussian;
  else if( cauchyLorentzButton.get_active() )
    return TheoreticalEnvelope::CauchyLorentz;
  else if( inverseCauchyLorentzButton.get_active() )
    return TheoreticalEnvelope::InverseCauchyLorentz;
}



bool EnvelopeDialog::setMaxLevel( double x )
{
  if( x < 0 ){
    Glib::ustring message( "the given level is less than zero" );
    Gbioinfo::displayMessage( message, Gtk::MESSAGE_ERROR );
    return false;
  }

  leftLevel.set_range( 0, x );
  rightLevel.set_range( 0, x );
  return true;
}

void EnvelopeDialog::setRightLevelMin()
{
  rightLevel.set_range( leftLevel.get_value(), leftAdjustment.get_upper() );
}

bool EnvelopeDialog::hasBlankEntry()
{
  if( lowerEntry.get_text_length() == 0 ){
    Gbioinfo::displayMessage( "lower is empty", Gtk::MESSAGE_ERROR );
    return true;
  }
  if( upperEntry.get_text_length() == 0 ){
    Gbioinfo::displayMessage( "upper is empty", Gtk::MESSAGE_ERROR );
    return true;
  }
  if( stepEntry.get_text_length() == 0 ){
    Gbioinfo::displayMessage( "step is empty", Gtk::MESSAGE_ERROR );
    return true;
  }
  if( rEntry.get_text_length() == 0 ){
    Gbioinfo::displayMessage( "resolution is empty", Gtk::MESSAGE_ERROR );
    return true;
  }
  if( normEntry.get_text_length() == 0 ){
    Gbioinfo::displayMessage( "normalization is empty", Gtk::MESSAGE_ERROR );
    return true;
  }
  return false;
}
