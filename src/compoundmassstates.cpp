#include <sstream>
#include <cmath>
#include <iomanip>
//#include <boost/lexical_cast.hpp>

#include <globalvar.hh>
#include <atom.hh>

//#include <gbioinfo/progressbardialog.hh>
#include "compoundmassstates.hh"

CompoundMassStates::CompoundMassStates
( const std::string &chemicalFormula ) : charge( 0 ), os( 0 ), te( 0 )
{
  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nEntering CompoundMassStates::CompoundMassStates ... " << std::endl;
  }
  composition.clear();
  std::map<std::string, unsigned int>
    atomNumber = Gbioinfo::ChemicalFormula::string2map( chemicalFormula );
  for( std::map<std::string, unsigned int>::const_iterator
	 it = atomNumber.begin(); it != atomNumber.end(); ++it ){
    Gbioinfo::ElementMassStates *ems =
      new Gbioinfo::ElementMassStates( it->first, it->second );

    if( Gbioinfo::gbioinfoDebug )
      std::cerr << *ems << std::endl;

    composition.push_back( ems );
  }

  // update maxTable
  maxTable.clear();
  maxTable.resize( composition.size(), 0 );

  for( std::vector<unsigned int>::size_type
	 i = maxTable.size() - 1; i >= 0; --i ){
    //std::cout << "i = " << i << std::endl;
    if( i == maxTable.size() - 1 )
      maxTable[ i ] = composition[ i ]->getLevelSize() - 1;
    else
      maxTable[ i ] = composition[ i ]->getLevelSize() - 1 +
	maxTable[ i + 1 ];

    // --0 = 4294967295
    if( i == 0 )
      break;
  }
  //std::cout << "updat maxTable successfully" << std::endl;

  //Gbioinfo::initializeElementAtomPool();
  // update meta information, like minNucleonNumber, maxNucleonNumber
  totalSpeciesNum = 1.;
  totalLevel = 0;
  minNucleonNumber = maxNucleonNumber = 0;
  maxLog10ProbLevel = 0;
  moleculeAverageMass = 0.;
  for( std::vector<const Gbioinfo::ElementMassStates*>::size_type
	 j = 0; j < composition.size(); ++j ){
    const Gbioinfo::ElementMassStates *ems = composition[ j ];
    // ground state
    minNucleonNumber += ems->getGroundState().nucleonNumber;
    // highest cited state
    maxNucleonNumber += ems->getHighestExcitedState().nucleonNumber;
    // max state level
    maxLog10ProbLevel += ems->getMaxStateLevel();

    totalLevel += ems->getLevelSize() - 1;
    totalSpeciesNum *= ems->getSpeciesSize();

    moleculeAverageMass += ems->getAtomNumber() *
      ( Gbioinfo::ElementAtomPool::instance()->getElement
	( ems->getElementSymbol() ).getAverageMass() );
  }
  totalLevel++;
  //std::cout << "exit update " << std::endl;

  currentSubLevel.clear();
  currentSubLevel.resize( composition.size() );

  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nLeaving CompoundMassStates::CompoundMassStates ... " << std::endl;
  }
}

CompoundMassStates::~CompoundMassStates()
{
  for( std::vector<const Gbioinfo::ElementMassStates*>::size_type
	 i = 0; i < composition.size(); ++i ){
    delete composition[ i ];
  }
}

void CompoundMassStates::setCharge( int c )
{
  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nEntering CompoundMassStates::setCharge ..." << std::endl;
  }
  charge = c;
  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nLeaving CompoundMassStates::setCharge ..." << std::endl;
  }
}

int CompoundMassStates::getCharge() const
{
  return charge;
}

void CompoundMassStates::outputGlobalInformation
( std::ostream &os, bool isMarkup ) const
{
  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nEntering CompoundMassStates::outputGlobalInformation ..." << std::endl;
  }
  if( isMarkup ){
    os << "<b>Summary of Chemical Molecule:</b>";
    for( std::vector<const Gbioinfo::ElementMassStates*>::size_type
	   i = 0; i < composition.size(); ++i ){
      os << composition[ i ]->getElementSymbol()
	 << "<sub>" << composition[ i ]->getAtomNumber() << "</sub>";
    }
    os << "\n"
       << "<b>AverageMass:</b> "
      //<< std::setw( Gbioinfo::ElementMassStates::getMassWidth() )
       << std::setprecision( Gbioinfo::ElementMassStates::getMassPrecision() )
       << std::fixed
       << moleculeAverageMass
       << "\n<b>Mass State Number:</b> "
       <<  std::setprecision( 0 )
       << std::noshowpoint << totalSpeciesNum
       << std::setprecision( Gbioinfo::ElementMassStates::getMassPrecision() )
       << "\n<b>Level Number:</b> "
       << totalLevel
       << "\n<b>Ground State:</b> ";
    setCurrentMoleculeMassState2GroundState();
    os << currentMoleculeMassStateNucleonNumber << "/"
       << std::setprecision( Gbioinfo::ElementMassStates::getMassPrecision() )
       << currentMoleculeMassStateMass << "/"
       << currentMoleculeMassStateLog10Prob
       << "\n<b>Highest Excited State:</b> ";
    setCurrentMoleculeMassState2HighestExcitedState();
    os << currentMoleculeMassStateNucleonNumber << "/"
       << currentMoleculeMassStateMass << "/"
       << currentMoleculeMassStateLog10Prob
       << "\n<b>Level of Maximum State:</b> "
      //<< getMaximumStateLevel()
       << maxLog10ProbLevel
       << std::endl;
  }
  else
    os << "Summary:\n"
    << "chemicalFormula "
    << getChemicalFormula()
    << std::endl
    << "averageMass "
    << moleculeAverageMass
    << std::endl
    << "totalSpeciesNumber "
    << totalSpeciesNum
    << std::endl
    << "totalLevelNumber "
    << totalLevel
    << std::endl
    << "groundStateNucleonNumber "
    << std::setw( Gbioinfo::ElementMassStates::getNucleonNumberWidth() )
    << minNucleonNumber
    << std::endl
    << "highestExcitedStateNucleonNumber "
    << std::setw( Gbioinfo::ElementMassStates::getNucleonNumberWidth() )
    << maxNucleonNumber
    << std::endl;

  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nLeaving CompoundMassStates::outputGlobalInformation ..." << std::endl;
  }
}

void CompoundMassStates::setCurrentMoleculeMassState2GroundState() const
{
  currentMoleculeMassState.clear();
  for( std::vector<const Gbioinfo::ElementMassStates*>::size_type
	 i = 0; i < composition.size(); ++i ){
    currentMoleculeMassState.push_back
      ( composition[ i ]->getGroundStatePtr() );
  }
  calculateCurrentMoleculeMassStateNucleonNumber();
  calculateCurrentMoleculeMassStateConfiguration();
  calculateCurrentMoleculeMassStateMass();
  calculateCurrentMoleculeMassStateLog10Prob();
}

void CompoundMassStates::setCurrentMoleculeMassState2HighestExcitedState() const
{
  currentMoleculeMassState.clear();
  for( std::vector<const Gbioinfo::ElementMassStates*>::size_type
	 i = 0; i < composition.size(); ++i ){
    currentMoleculeMassState.push_back
      ( composition[ i ]->getHighestExcitedStatePtr() );
  }
  calculateCurrentMoleculeMassStateNucleonNumber();
  calculateCurrentMoleculeMassStateConfiguration();
  calculateCurrentMoleculeMassStateMass();
  calculateCurrentMoleculeMassStateLog10Prob();
}

void CompoundMassStates::calculateCurrentMoleculeMassStateNucleonNumber() const
{
  currentMoleculeMassStateNucleonNumber = 0;
  for( std::vector<const Gbioinfo::ElementMassStates::MassState*>::size_type
	 i = 0; i < currentMoleculeMassState.size(); ++i )
    currentMoleculeMassStateNucleonNumber +=
      currentMoleculeMassState[ i ]->nucleonNumber;
}


void CompoundMassStates::calculateCurrentMoleculeMassStateConfiguration() const
{
  currentMoleculeMassStateConfiguration.clear();
  for( std::vector<const Gbioinfo::ElementMassStates::MassState*>::size_type
	 i = 0; i < currentMoleculeMassState.size(); ++i )
    for( std::vector<unsigned int>::size_type j = 0;
	 j < currentMoleculeMassState[ i ]->configuration.size(); ++j )
      currentMoleculeMassStateConfiguration.push_back
	( currentMoleculeMassState[ i ]->configuration[ j ] );
}

void CompoundMassStates::calculateCurrentMoleculeMassStateMass() const
{
  currentMoleculeMassStateMass = 0.;
  for( std::vector<const Gbioinfo::ElementMassStates::MassState*>::size_type
	 i = 0; i < currentMoleculeMassState.size(); ++i )
    currentMoleculeMassStateMass +=
      currentMoleculeMassState[ i ]->mass;
  if( charge != 0 )
    currentMoleculeMassStateMass /= fabs( charge );
}

void CompoundMassStates::calculateCurrentMoleculeMassStateLog10Prob() const
{
  currentMoleculeMassStateLog10Prob = 0.;
  for( std::vector<const Gbioinfo::ElementMassStates::MassState*>::size_type
	 i = 0; i < currentMoleculeMassState.size(); ++i )
    currentMoleculeMassStateLog10Prob +=
      currentMoleculeMassState[ i ]->log10Prob;
}

const std::string CompoundMassStates::getChemicalFormula() const
{
  std::ostringstream oss;
  for( std::vector<const Gbioinfo::ElementMassStates*>::size_type
         i = 0; i < composition.size(); ++i ){
    oss << composition[ i ]->getElementSymbol()
	<< composition[ i ]->getAtomNumber();
  }
  return oss.str();
}

unsigned int CompoundMassStates::getHighestExcitedStateLevel() const
{
  unsigned int n = 0;
  for( std::vector<const Gbioinfo::ElementMassStates*>::size_type
	 i = 0; i < composition.size(); ++i ){
    n += composition[ i ]->getHighestExcitedState().getLevel();
  }

  if( Gbioinfo::gbioinfoDebug ){
    std::cout << "the level of current molecule highest excited state: "
	      << n << std::endl;
  }
  return n;
}

unsigned int CompoundMassStates::getMaximumStateLevel() const
{
  unsigned int n = 0;
  for( std::vector<const Gbioinfo::ElementMassStates*>::size_type
	 i = 0; i < composition.size(); ++i )
    n += composition[ i ]->getMaxStateLevel();

  return n;
}

void CompoundMassStates::calculateGrossStick
( std::deque<std::pair<unsigned int, double> > &grossStick, double threshold ) const
{
  unsigned int upper = getHighestExcitedStateLevel();
  grossStick.clear();
  double culmutativeAbundance = 0.;
  bool isLeft = true;
  unsigned int n = maxLog10ProbLevel;

  do{
    navigateOneLevel( n, LEVELTOTALABUNDANCE );
    culmutativeAbundance += currentLevelTotalAbundance;
    if( isLeft )
      grossStick.push_front
	( std::make_pair( n, currentLevelTotalAbundance ) );
    else
      grossStick.push_back
	( std::make_pair( n, currentLevelTotalAbundance ) );

    unsigned int leftLevel = grossStick[ 0 ].first;
    double leftAbundance = grossStick[ 0 ].second;
    unsigned int rightLevel = grossStick[ grossStick.size() - 1 ].first;
    double rightAbundance = grossStick[ grossStick.size() - 1 ].second;

    if( leftAbundance >= rightAbundance ){
      if( leftLevel != 0 ){
	n = leftLevel - 1; isLeft = true;
      }
      else if( rightLevel != upper ){
	n = rightLevel + 1; isLeft = false;
      }
      else
	break;
    }
    else{
      if( rightLevel != upper ){
	n = rightLevel + 1; isLeft = false;
      }
      else if( leftLevel != 0 ){
	n = leftLevel - 1; isLeft = true;
      }
      else
	break;
    }
  }while( culmutativeAbundance < threshold );
}

void CompoundMassStates::
calculateSpeciesNumberOfGrossStructure( std::vector<double> &number )
{
  number.clear();
  unsigned int upper = getHighestExcitedStateLevel();
  for( unsigned int i = 0; i <= upper; ++i ){
    navigateOneLevel( i, LEVELSTATENUM );
    number.push_back( currentLevelStateNumber );
  }
}





void CompoundMassStates::setOutputStream( std::ostream *os )
{
  this->os = os;
}

bool CompoundMassStates::setOutputStream( std::string filename )
{
  if( os ){
    os->clear();
  }

  os = new std::ofstream( filename.c_str() );
  if( ! (*os) ){
    std::string message( "Error: set output file failed" );
    Gbioinfo::displayMessage( message, Gtk::MESSAGE_ERROR );
    return false;
  }
  return true;
}

void CompoundMassStates::setEnvelopePtr( TheoreticalEnvelope *te )
{
  this->te = te;
}

void CompoundMassStates::navigateOneLevel
( unsigned int level, NavigateLevelAction action ) const
{
  if( Gbioinfo::gbioinfoDebug ){
    std::cout << "Navigate level " << level << std::endl;
  }

  currentLevel = level;
  currentLevelStateNumber = 0.;
  currentLevelTotalAbundance = 0.;
  currentLevelAverageMass = 0.;
  currentAction = action;

  if( level > maxTable[ 0 ] ){
    std::string message( "Error: level beyond the max, set to the max" );
    Gbioinfo::displayMessage( message, Gtk::MESSAGE_ERROR );
    level = maxTable[ 0 ];
  }

  navigateOneLevelRecursive( level, 0 );

  if( currentAction & LEVELSTATENUM ){
    /*
    (*os) << std::setw( Gbioinfo::ElementMassStates::getLevelWidth() )
	  << currentLevel
	  << std::setw( Gbioinfo::ElementMassStates::getLevelWidth() )
	  << std::fixed << std::noshowpoint << std::setprecision( 0 )
	  << currentLevelStateNumber
	  << std::endl;
    */
  }

  if( currentAction & LEVELAVERAGEMASS )
    currentLevelAverageMass /= currentLevelTotalAbundance;

  if( Gbioinfo::gbioinfoDebug ){
    std::cout << "Navigate level " << level << " done" << std::endl;
  }
}

void CompoundMassStates::navigateOneLevelRecursive
( unsigned int level, unsigned int elementIndex ) const
{
  //std::cout << "enter navigateOneLevelRecursive" << std::endl;
  if( elementIndex == composition.size() - 1 ){
    currentSubLevel[ elementIndex ] = level;

    if( Gbioinfo::gbioinfoDebug ){
      std::cout << "navigate sublevel: ";
      for( std::vector<unsigned int>::size_type i = 0; i < currentSubLevel.size(); ++i ){
	std::cout << "\t" << currentSubLevel[ i ];
      }
      std::cout << std::endl;
    }

    if( currentAction & LEVELSTATENUM ){
      double stateNum = 1.;
      for( std::vector<unsigned int>::size_type
	     i = 0; i < currentSubLevel.size(); ++i ){
	stateNum *= composition[ i ]->getSubLevelSize( currentSubLevel[ i ] );
      }
      currentLevelStateNumber += stateNum;
    }

    if( ( currentAction & LEVELTOTALABUNDANCE ) ||
	( currentAction & LEVELAVERAGEMASS ) ||
	( currentAction & EACHSTATEOUTPUT ) ||
	( currentAction & ENVELOPE ) ){
      currentMoleculeMassState.clear();
      currentMoleculeMassState.resize( composition.size() );
      navigateSubLevelRecursive( 0 );
    }

    if( Gbioinfo::gbioinfoDebug ){
      std::cout << "navigate sublevel done" << std::endl;
    }
    return;
  }

  unsigned int minLevel, maxLevel;
  if( level > maxTable[ elementIndex + 1 ] )
    minLevel = level - maxTable[ elementIndex + 1 ];
  else
    minLevel = 0;

  maxLevel = level;
  if( level > maxTable[ elementIndex ] - maxTable[ elementIndex + 1 ] )
    maxLevel = maxTable[ elementIndex ] - maxTable[ elementIndex + 1 ];

  //std::cout << "min and max level " << minLevel << ' ' << maxLevel << std::endl;
  for( unsigned int i = minLevel; i <= maxLevel; ++i ){
    currentSubLevel[ elementIndex ] = i;
    navigateOneLevelRecursive( level - i, elementIndex + 1 );
  }
  //std::cout << "exit navigateOneLevelRecursive" << std::endl;
}

void CompoundMassStates::navigateSubLevelRecursive
( unsigned int elementIndex ) const
{
  std::vector<Gbioinfo::ElementMassStates::MassState*> msPtrs;
  composition[ elementIndex ]->getOneLevelMassStates
    ( currentSubLevel[ elementIndex ], msPtrs );

  for( std::vector<Gbioinfo::ElementMassStates::MassState*>::size_type
	 i = 0; i < msPtrs.size(); ++i ){
    currentMoleculeMassState[ elementIndex ] = msPtrs[ i ];

    if( elementIndex == composition.size() - 1 ){
      if( Gbioinfo::gbioinfoDebug ){
	std::cout << "navigate mass states in sub level " << std::endl;
      }

      calculateCurrentMoleculeMassStateNucleonNumber();
      calculateCurrentMoleculeMassStateConfiguration();
      calculateCurrentMoleculeMassStateMass();
      calculateCurrentMoleculeMassStateLog10Prob();

      if( currentAction & LEVELTOTALABUNDANCE ){
	if( currentMoleculeMassStateLog10Prob > -20. ){
	  double abundance = pow( 10., currentMoleculeMassStateLog10Prob );
	  currentLevelTotalAbundance += abundance;
	}
      }

      if( currentAction & ENVELOPE ){
	te->calculateSingleSpecies
	  ( currentMoleculeMassStateMass, currentMoleculeMassStateLog10Prob, true );
      }

      // output each state
      if( ( currentAction & EACHSTATEOUTPUT ) ){
	//unsigned int charge2;
	//if( charge < 0 )
	//charge2 = -charge;
	//else if( charge == 0 )
	//charge2 = 1;
	//else
	//charge2 = charge;

	(*os) << std::setw( Gbioinfo::ElementMassStates::getLevelWidth() )
	      << currentLevel
	      << std::setw( Gbioinfo::ElementMassStates::getNucleonNumberWidth() )
	      << currentMoleculeMassStateNucleonNumber;
	for( std::vector<unsigned int>::size_type j = 0;
	     j < currentMoleculeMassStateConfiguration.size(); ++j )
	  (*os) << std::setw( Gbioinfo::ElementMassStates::getConfigurationWidth() )
		<< currentMoleculeMassStateConfiguration[ j ];
	(*os) << std::setw( Gbioinfo::ElementMassStates::getMassWidth() )
	      << std::setprecision( Gbioinfo::ElementMassStates::getMassPrecision() )
	  //<< currentMoleculeMassStateMass / charge2
	      << currentMoleculeMassStateMass
	      << std::setw( Gbioinfo::ElementMassStates::getAbundanceWidth() )
	      << std::setprecision( Gbioinfo::ElementMassStates::getAbundancePrecision() )
	      << currentMoleculeMassStateLog10Prob
	      << std::endl;
      }
    }
    else{
      navigateSubLevelRecursive( elementIndex + 1 );
    }
  }

  //std::cout << "exit navigateSubLevelRecursive" << std::endl;
}

void CompoundMassStates::outputCurrentMoleculeMassState() const
{
  unsigned int charge2;
  if( charge < 0 )
    charge2 = -charge;
  else if( charge == 0 )
    charge2 = 1;
  else
    charge2 = charge;

  (*os) << std::setw( Gbioinfo::ElementMassStates::getLevelWidth() )
	<< currentLevel
	<< std::setw( Gbioinfo::ElementMassStates::getNucleonNumberWidth() )
	<< currentMoleculeMassStateNucleonNumber;

  for( std::vector<const Gbioinfo::ElementMassStates::MassState*>::size_type
	 i = 0; i < currentMoleculeMassState.size(); ++i ){
    const std::vector<unsigned int> &configuration =
      currentMoleculeMassState[ i ]->configuration;
    for( std::vector<unsigned int>::size_type j = 0;
	 j < configuration.size(); ++j )
      (*os) << std::setw( Gbioinfo::ElementMassStates::getConfigurationWidth() )
	    << configuration[ j ];
  }

  (*os) << std::setw( Gbioinfo::ElementMassStates::getMassWidth() )
	<< std::setprecision( Gbioinfo::ElementMassStates::getMassPrecision() )
	<< currentMoleculeMassStateMass / charge2
	<< std::setw( Gbioinfo::ElementMassStates::getAbundanceWidth() )
	<< std::setprecision( Gbioinfo::ElementMassStates::getAbundancePrecision() )
	<< currentMoleculeMassStateLog10Prob
	<< std::endl;
}

