#ifndef GBIOINFO_GLOBALVAR_HH
#define GBIOINFO_GLOBALVAR_HH

//#include <string>
#include <gtkmm.h>

namespace Gbioinfo
{
  // two running mode: command line mode (GBIOINFO_CML)
  // and GUI mode (GBIOINFO_GTKMM)
  enum RunMode {GBIOINFO_CML = 1, GBIOINFO_GTKMM = 2};
  extern RunMode gbioinfoRunMode;

  // in default, the warning and the debug information are both turned off
  extern bool gbioinfoWarning;
  extern bool gbioinfoDebug;

  // output message to standard error (in command line mode)
  // or using a dialog in GUI mode
  void displayMessage( const Glib::ustring &message, Gtk::MessageType type );

}//namespace

#endif
