#include <iostream>
#include <iomanip>

//#include <sys/times.h>

//#include <boost/lexical_cast.hpp>

#include <atom.hh>
#include <globalvar.hh>

#include "mainwindow.hh"
#include "theoreticalenvelope.hh"
#include "grossstickdialog.hh"
#include "envelopedialog.hh"
#include "massstatesleveldialog.hh"

MainWindow::MainWindow()
  : clearButton( Gtk::Stock::CLEAR ),
    chargeLabel( "Charge: " ),
    chargeAdjustment( 0, -50, 50, 1, 1, 0 ),
    chargeSpinButton( chargeAdjustment ),
    cms( 0 ), te( 0 )
{
  if( Gbioinfo::gbioinfoDebug )
    std::cerr << "\nEnter MainWindow::MainWindow ..." << std::endl;

  actionGroup = Gtk::ActionGroup::create();
  // actions in file menue
  actionGroup->add( Gtk::Action::create( "FileMenu", "_File" ) );

  actionFileLoadIsotope = 
    Gtk::Action::create( "FileLoadIsotope", "Load isotope file ..." );
  actionGroup->add
    ( actionFileLoadIsotope,
      sigc::mem_fun( *this, &MainWindow::onActionFileLoadIsotopeFile ) );

  actionGroup->add( Gtk::Action::create( "FileExport", "Export" ) );
  actionFileExportGrossStick =
    Gtk::Action::create( "FileExportGrossStick", "Gross Stick" );
  actionGroup->add
    ( actionFileExportGrossStick,
      sigc::mem_fun( *this, &MainWindow::onActionFileExportGrossStick ) );

  actionFileExportGrossSpeciesNumber =
    Gtk::Action::create( "FileExportGrossSpeciesNumber", "Gross Species Number" );
  actionGroup->add
    ( actionFileExportGrossSpeciesNumber,
      sigc::mem_fun( *this, &MainWindow::onActionFileExportGrossSpeciesNumber ) );

  actionFileExportFineStructures =
    Gtk::Action::create( "FileExportFineStructures", "Fine Structures" );
  actionGroup->add
    ( actionFileExportFineStructures,
      sigc::mem_fun( *this, &MainWindow::onActionFileExportFineStructures ) );

  actionFileExportEnvelope =
    Gtk::Action::create( "FileExportEnvelope", "Envelope" );
  actionGroup->add
    ( actionFileExportEnvelope,
      sigc::mem_fun( *this, &MainWindow::onActionFileExportEnvelope ) );

  actionGroup->add
    ( Gtk::Action::create( "FileQuit", Gtk::Stock::QUIT ),
      sigc::mem_fun( *this, &MainWindow::onActionFileQuit ) );

  // actions in calculate menu
  actionGroup->add( Gtk::Action::create( "CalculateMenu", "_Calculate" ) );
  actionCalculateGrossStick =
    Gtk::Action::create( "CalculateGrossStick", "Gross stick" );
  actionGroup->add
    ( actionCalculateGrossStick,
      sigc::mem_fun( *this, &MainWindow::onActionCalculateGrossStick ) );

  actionCalculateGrossSpeciesNumber =
    Gtk::Action::create( "CalculateGrossSpeciesNumber", "Gross Species Number" );
  actionGroup->add
    ( actionCalculateGrossSpeciesNumber,
      sigc::mem_fun( *this, &MainWindow::onActionCalculateGrossSpeciesNumber ) );

  //actionGroup->add
  //  ( Gtk::Action::create( "CalculateFineStructures", "Fine Structures" ),
  //    sigc::mem_fun( *this, &MainWindow::onActionCalculateFineStuctures ) );

  actionCalculateEnvelope =
    Gtk::Action::create( "CalculateEnvelope", "Envelope" );
  actionGroup->add
    ( actionCalculateEnvelope,
      sigc::mem_fun( *this, &MainWindow::onActionCalculateEnvelope ) );

  // actions in help menu
  actionGroup->add( Gtk::Action::create( "HelpMenu", "_Help" ) );
  actionHelpAbout =
    Gtk::Action::create( "About", Gtk::Stock::ABOUT );
  actionGroup->add
    ( actionHelpAbout,
      sigc::mem_fun( *this, &MainWindow::onActionHelpAbout ) );

  uiManager = Gtk::UIManager::create();
  uiManager->insert_action_group( actionGroup );
  add_accel_group( uiManager->get_accel_group() );

  actionFileExportGrossStick->set_sensitive( false );
  actionFileExportGrossSpeciesNumber->set_sensitive( false );
  actionFileExportFineStructures->set_sensitive( false );
  actionFileExportEnvelope->set_sensitive( false );
  actionCalculateGrossStick->set_sensitive( false );
  actionCalculateGrossSpeciesNumber->set_sensitive( false );
  actionCalculateEnvelope->set_sensitive( false );

  Glib::ustring uiInfo =
    "<ui>"
    "  <menubar name='MenuBar'>"
    "    <menu action='FileMenu'>"
    "      <menuitem action='FileLoadIsotope'/>"
    "      <menu action='FileExport'>"
    "        <menuitem action='FileExportGrossStick'/>"
    "        <menuitem action='FileExportGrossSpeciesNumber'/>"
    "        <menuitem action='FileExportFineStructures'/>"
    "        <menuitem action='FileExportEnvelope'/>"
    "      </menu>"
    "      <separator/>"
    "      <menuitem action='FileQuit'/>"
    "    </menu>"
    "    <menu action='CalculateMenu'>"
    "      <menuitem action='CalculateGrossStick'/>"
    "      <menuitem action='CalculateGrossSpeciesNumber'/>"
    //"      <menuitem action='CalculateFineStructures'/>"
    "      <menuitem action='CalculateEnvelope'/>"
    "    </menu>"
    "    <menu action='HelpMenu'>"
    "      <menuitem action='About'/>"
    "    </menu>"
    "  </menubar>"
    "</ui>";

  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  try{
    uiManager->add_ui_from_string( uiInfo );
  }
  catch( const Glib::Error &ex ){
    std::cerr << "building menus failed: " << ex.what();
  }
  #else
  std::auto_ptr<Glib::Error> ex;
  uiManager->add_ui_from_string( uiInfo, ex );
  if( ex.get() ){
    std::cerr << "building menus failed: " << ex->what();
  }
  #endif

  set_title( "Isotope Calculator" );
  set_default_size( 600, 600 );

  Gtk::Widget *pMenuBar = uiManager->get_widget( "/MenuBar" );
  if( pMenuBar )
    vBox.pack_start( *pMenuBar, Gtk::PACK_SHRINK );

  chemicalFormulaEntry.signal_activate().connect
    (sigc::mem_fun( *this, &MainWindow::onChemicalFormulaEntryActivate ) );

  clearButton.signal_clicked().connect
    (sigc::mem_fun( *this, &MainWindow::onClearButtonClicked ) );

  chargeSpinButton.signal_value_changed().connect
    ( sigc::mem_fun( *this, &MainWindow::onChargeSpinButtonValueChanged ) );

  hBox.pack_start( chemicalFormulaEntry );
  hBox.pack_start( clearButton, Gtk::PACK_SHRINK );
  hBox.pack_start( chargeLabel, Gtk::PACK_SHRINK );
  hBox.pack_start( chargeSpinButton, Gtk::PACK_SHRINK );
  vBox.pack_start( hBox, Gtk::PACK_SHRINK );

  hBox2.pack_start( summaryLabel, Gtk::PACK_SHRINK );
  //hBox2.pack_start( gsh, Gtk::PACK_SHRINK );
  vBox.pack_start( hBox2 );

  scrolledWindow.set_policy( Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC );
  vBox.pack_start( scrolledWindow );
  scrolledWindow.add( isotopeView );
  setIsotopeColumnsData();

  add( vBox );
  show_all_children();

  if( Gbioinfo::gbioinfoDebug )
    std::cerr << "\nLeave MainWindow::MainWindow ..." << std::endl;
}

MainWindow::~MainWindow()
{}

void MainWindow::onActionFileLoadIsotopeFile()
{
  Gtk::FileChooserDialog dialog
    ( "Choose isotope file", Gtk::FILE_CHOOSER_ACTION_OPEN );
  dialog.add_button( Gtk::Stock::OK, Gtk::RESPONSE_OK );
  dialog.add_button( Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );
  dialog.add_button( "Set Default", 0 );
  int result = dialog.run();

  std::string filename;
  switch( result ){
  case Gtk::RESPONSE_OK :
    filename = dialog.get_filename();
    break;
  case 0:
    filename = "";
    break;
  case Gtk::RESPONSE_CANCEL :
    return;
  }

  setIsotopeColumnsData( filename );

  onClearButtonClicked();
}

void MainWindow::onActionFileExportGrossStick()
{
  std::string filename;
  {
    Gtk::FileChooserDialog dialog
      ( "Save gross stick", Gtk::FILE_CHOOSER_ACTION_SAVE );
    dialog.add_button( Gtk::Stock::OK, Gtk::RESPONSE_OK );
    dialog.add_button( Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );
    dialog.set_do_overwrite_confirmation( true );
    std::ostringstream oss;
    oss << '_' << grossStickThreshold;
    std::string str( oss.str() );
    std::replace( str.begin(), str.end(), '.', 'd' );
    oss.str( "" );
    oss << '_' << grossStick[ 0 ].first
	<< '_' << grossStick[ grossStick.size() - 1 ].first
	<< ".gs";
    oss.str( cms->getChemicalFormula() + str + oss.str() );
    dialog.set_current_name( oss.str() );
    int result = dialog.run();

    switch( result ){
    case Gtk::RESPONSE_OK :
      filename = dialog.get_filename();
      break;
    case Gtk::RESPONSE_CANCEL :
      return;
    }
  }
  std::ofstream outFile( filename.c_str() );
  for( std::deque<std::pair<unsigned int, double> >::size_type i = 0;
       i < grossStick.size(); ++i )
    outFile << std::setw( Gbioinfo::ElementMassStates::getLevelWidth() )
	    << grossStick[ i ].first
	    << std::fixed
	    << std::setw( Gbioinfo::ElementMassStates::getAbundanceWidth() )
	    << std::setprecision( Gbioinfo::ElementMassStates::getAbundancePrecision() )
	    << grossStick[ i ].second
	    << std::endl;
}

void MainWindow::onActionFileExportGrossSpeciesNumber()
{
  std::string filename;
  {
    Gtk::FileChooserDialog dialog
      ( "Save gross species number", Gtk::FILE_CHOOSER_ACTION_SAVE );
    dialog.add_button( Gtk::Stock::OK, Gtk::RESPONSE_OK );
    dialog.add_button( Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );
    dialog.set_do_overwrite_confirmation( true );
    dialog.set_current_name( cms->getChemicalFormula() + ".gsn" );
    int result = dialog.run();
    switch( result ){
    case Gtk::RESPONSE_OK :
      filename = dialog.get_filename();
      break;
    case Gtk::RESPONSE_CANCEL :
      return;
    }
  }
  std::ofstream outFile( filename.c_str() );
  for( std::vector<double>::size_type i = 0;
       i < grossSpeciesNumber.size(); ++i )
    outFile << std::setw( Gbioinfo::ElementMassStates::getLevelWidth() )
	    << i << "\t"
	    << std::fixed
	    << std::noshowpoint
	    << std::setprecision( 0 )
	    << grossSpeciesNumber[ i ]
	    << std::endl;
}

void MainWindow::onActionFileExportFineStructures()
{
  unsigned int leftValue, rightValue;
  {
    MassStatesLevelDialog levelDialog;
    double maxValue = cms->getHighestExcitedStateLevel();
    levelDialog.setLeftRange( 0, maxValue );
    levelDialog.setRightRange( 0, maxValue );
    levelDialog.show_all_children();
    int result = levelDialog.run();
    switch( result ){
    case Gtk::RESPONSE_OK :
      leftValue = static_cast<unsigned int>( levelDialog.getLeftValue() );
      rightValue = static_cast<unsigned int>( levelDialog.getRightValue() );
      break;
    case Gtk::RESPONSE_CANCEL :
      return;
    }
  }
  std::string filename;
  {
    Gtk::FileChooserDialog dialog
      ( "Save gross stick", Gtk::FILE_CHOOSER_ACTION_SAVE );
    dialog.add_button( Gtk::Stock::OK, Gtk::RESPONSE_OK );
    dialog.add_button( Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );
    dialog.set_do_overwrite_confirmation( true );
    std::ostringstream oss;
    oss << '_' << cms->getCharge()
	<< '_' << leftValue
	<< '_' << rightValue
	<< ".fs";
    oss.str( cms->getChemicalFormula() + oss.str() );
    dialog.set_current_name( oss.str() );
    int result = dialog.run();
    switch( result ){
    case Gtk::RESPONSE_OK :
      filename = dialog.get_filename();
      break;
    case Gtk::RESPONSE_CANCEL :
      return;
    }
  }

  std::ofstream outFile( filename.c_str() );
  cms->setOutputStream( &outFile );
  for( unsigned int i = leftValue; i <= rightValue; ++i )
    cms->navigateOneLevel( i, CompoundMassStates::EACHSTATEOUTPUT );
  cms->setOutputStream( 0 );
}

void MainWindow::onActionFileExportEnvelope()
{
  std::string filename;
  {
    Gtk::FileChooserDialog dialog
      ( "Save theoretical envelope", Gtk::FILE_CHOOSER_ACTION_SAVE );
    dialog.add_button( Gtk::Stock::OK, Gtk::RESPONSE_OK );
    dialog.add_button( Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );
    dialog.set_do_overwrite_confirmation( true );
    std::ostringstream oss;
    oss << '_' << cms->getCharge()
	<< '_' << leftLevel
	<< '_' << rightLevel
	<< std::fixed << std::setprecision( 8 )
	<< '_' << te->getLower()
	<< '_' << te->getUpper()
	<< '_' << te->getStep()
	<< '_' << te->getResolution()
	<< '_' << te->getNormalization()
	<< '_' << te->getFunctionType();
    std::string str( cms->getChemicalFormula() + oss.str() );
    std::replace( str.begin(), str.end(), '.', 'd' );
    str += ".te";
    dialog.set_current_name( str );
    int result = dialog.run();
    switch( result ){
    case Gtk::RESPONSE_OK :
      filename = dialog.get_filename();
      break;
    case Gtk::RESPONSE_CANCEL :
      return;
    }
  }

  std::ofstream outFile( filename.c_str() );
  outFile << (*te) << std::endl;
}

void MainWindow::onActionFileQuit()
{
}

void MainWindow::onActionCalculateGrossStick()
{
  if( ! grossStick.empty() )
    return;

  {
    std::stringstream ss;
    double x;

    GrossStickDialog dialog( "(^0?\\.\\d*$|^[01]?$)" );
    int result = dialog.run();
    switch( result ){
    case Gtk::RESPONSE_OK:
      //grossStickThreshold = boost::lexical_cast<double>( dialog.getText() );
      ss.str( dialog.getText() );
      ss >> x;
      grossStickThreshold = x;
      break;
    case Gtk::RESPONSE_CANCEL:
      return;
    }
  }

  cms->calculateGrossStick( grossStick, grossStickThreshold );
  actionFileExportGrossStick->set_sensitive( true );
}

void MainWindow::onActionCalculateGrossSpeciesNumber()
{
  if( ! grossSpeciesNumber.empty() )
    return;

  cms->calculateSpeciesNumberOfGrossStructure( grossSpeciesNumber );
  actionFileExportGrossSpeciesNumber->set_sensitive( true );
}

void MainWindow::onActionCalculateEnvelope()
{
  double lower, upper, step, r, norm;
  TheoreticalEnvelope::ConvolutionFunctionType type;
  //int left, right;
  {
    EnvelopeDialog dialog;
    dialog.setMaxLevel( cms->getHighestExcitedStateLevel() );
    dialog.set_transient_for( *this );
    while( int result = dialog.run() ){
      switch( result ){
      case Gtk::RESPONSE_OK:
	if( dialog.hasBlankEntry() )
	  continue;
	lower = dialog.getLower();
	upper = dialog.getUpper();
	step = dialog.getStep();
	r = dialog.getResolution();
	norm = dialog.getNormalization();
	type = dialog.getType();
	leftLevel = dialog.getLeftLevel();
	rightLevel = dialog.getRightLevel();
	break;
      case Gtk::RESPONSE_CANCEL:
	return;
      }
      break;
    }
  }
  if( ! te ){
    te = new TheoreticalEnvelope( lower, upper, step, r, norm, type );
    cms->setEnvelopePtr( te );
  }
  else{
    te->setLower( lower );
    te->setUpper( upper );
    te->setStep( step );
    te->setResolution( r );
    te->setNormalization( norm );
    te->setFunctionType( type );
    te->resetEnvelopeZero();
  }

  //struct tms start, end;
  //times( &start );
  for( unsigned int i = leftLevel; i <= rightLevel; ++i ){
    cms->navigateOneLevel( i, CompoundMassStates::ENVELOPE );
  }
  //times( &end );
  /*
  std::cerr << "Calculation time of theoretical envelope: "
	    << std::setprecision( 6 )
	    << ( end.tms_utime - start.tms_utime ) / (double)sysconf(_SC_CLK_TCK) << std::endl;
  */
  actionFileExportEnvelope->set_sensitive( true );
}

void MainWindow::onActionHelpAbout()
{
  Gtk::MessageDialog dialog( *this, "Isotopecalculator version 1.0." );
  dialog.set_title( "About isotopecalculator" );
  int result = dialog.run();
  switch( result ){
  case Gtk::RESPONSE_OK :
    return;
  }
}

void MainWindow::setIsotopeColumnsData( const std::string &filename )
{
  if( Gbioinfo::gbioinfoDebug )
    std::cerr << "\nEnter MainWindow::setIsotopeColumnsData ..." << std::endl;

  if( filename.empty() )
    Gbioinfo::ElementAtomPool::instance()->loadAtoms();
  else
    Gbioinfo::ElementAtomPool::instance()->loadAtoms( filename );

  if( isotopeColumnsRef ){
    isotopeColumnsRef->clear();
  }
  else{
    isotopeColumnsRef = Gtk::ListStore::create( isotopeColumns );
    isotopeView.set_model( isotopeColumnsRef );
    isotopeView.append_column_numeric
      ( "atomic number", isotopeColumns.atomicNumber, "%3d" );
    isotopeView.append_column( "symbol", isotopeColumns.elementName );
    isotopeView.append_column_numeric
      ( "mass number", isotopeColumns.massNumber, "%3d" );
    isotopeView.append_column_numeric
      ( "mass", isotopeColumns.mass, "%.8f" );
    isotopeView.append_column_numeric
      ( "abundance", isotopeColumns.abundance, "%.8f" );
  }

  std::vector<unsigned int> atomicNumbers;
  Gbioinfo::ElementAtomPool::instance()->getAllAtomicNumber( atomicNumbers );
  for( std::vector<unsigned int>::size_type i = 0; i != atomicNumbers.size(); ++i ){
    const Gbioinfo::Element &element =
      Gbioinfo::ElementAtomPool::instance()->getElement( atomicNumbers[ i ] );
    for( unsigned int j = 0; j != element.getNumberOfIsotopes(); ++j ){
      const Gbioinfo::Atom &isotope = element.getIsotope( j, Gbioinfo::Element::IsotopeIndex );
      Gtk::TreeModel::Row row = *( isotopeColumnsRef->append() );
      row[ isotopeColumns.atomicNumber ] = element.atomicNumber;
      row[ isotopeColumns.elementName ] = element.symbol;
      row[ isotopeColumns.massNumber ] = isotope.getMassNumber();
      row[ isotopeColumns.mass ] = isotope.mass;
      row[ isotopeColumns.abundance ] = isotope.abundance;
    }
  }

  if( Gbioinfo::gbioinfoDebug )
    std::cerr << "\nLeave MainWindow::setIsotopeColumnsData ..." << std::endl;

}

void MainWindow::onChemicalFormulaEntryActivate()
{
  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nEntering MainWindow::onChemicalFormulaEntryActivate ..." << std::endl;
  }
  actionFileExportGrossStick->set_sensitive( false );
  actionFileExportGrossSpeciesNumber->set_sensitive( false );
  actionFileExportFineStructures->set_sensitive( true );
  actionFileExportEnvelope->set_sensitive( false );
  actionCalculateGrossStick->set_sensitive( true );
  actionCalculateGrossSpeciesNumber->set_sensitive( true );
  actionCalculateEnvelope->set_sensitive( true );

  grossStick.clear();
  grossSpeciesNumber.clear();

  if( cms ){
    delete cms;
    cms = 0;
    delete te;
    te = 0;
  }

  std::string str( chemicalFormulaEntry.get_text() );
  try{
    //struct tms start, end;
    //times( &start );
    cms = new CompoundMassStates( str );
    //times( &end );
    /*
    std::cerr << "Calculation time of all elements' hierarchical structure: "
	      << std::setprecision( 6 )
	      << ( end.tms_utime - start.tms_utime ) / (double)sysconf(_SC_CLK_TCK) << std::endl;
    */
  }
  catch( std::runtime_error err ){
    cms = 0;
    summaryLabel.set_markup( "" );
    set_title( std::string( "Isotope Calculator -- formula error" ) );
    return;
  }
  cms->setCharge( static_cast<int>( chargeSpinButton.get_value() ) );

  std::ostringstream oss;
  cms->outputGlobalInformation( oss, true );
  summaryLabel.set_markup( oss.str() );

  set_title( std::string( "Isotope Calculator -- " ) + str );
  if( Gbioinfo::gbioinfoDebug ){
    std::cerr << "\nLeaving MainWindow::onChemicalFormulaEntryActivate ..." << std::endl;
  }
}

void MainWindow::onChargeSpinButtonValueChanged()
{
  if( ! cms )
    return;

  cms->setCharge( static_cast<int>( chargeSpinButton.get_value() ) );
  std::ostringstream oss;
  cms->outputGlobalInformation( oss, true );
  summaryLabel.set_markup( oss.str() );
}

void MainWindow::onClearButtonClicked()
{
  actionFileExportGrossStick->set_sensitive( false );
  actionFileExportGrossSpeciesNumber->set_sensitive( false );
  actionFileExportFineStructures->set_sensitive( false );
  actionFileExportEnvelope->set_sensitive( false );
  actionCalculateGrossStick->set_sensitive( false );
  actionCalculateGrossSpeciesNumber->set_sensitive( false );
  actionCalculateEnvelope->set_sensitive( false );
  if( cms ){
    delete cms;
    cms = 0;
    delete te;
    te = 0;
  }
  chemicalFormulaEntry.set_text( "" );
  //chemicalFormulaEntry.set_editable( true );

  summaryLabel.set_text( "" );
  set_title( "Isotope Calculator -- no molecule" );

  chargeSpinButton.set_value( 0. );
}
