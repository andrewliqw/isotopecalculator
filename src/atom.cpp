#include <fstream>

#include "globalvar.hh"
#include "atom.hh"

namespace Gbioinfo{

  ElementAtomPool *ElementAtomPool::pool = NULL;


  unsigned int Element::getNumberOfIsotopes() const
  {
    if( Gbioinfo::gbioinfoDebug ){
      std::cerr << "\nEnter Element::getNumberOfIsotopes ..." << std::endl;
    }

    std::map<std::string, std::vector<unsigned int> >::const_iterator
      it = ElementAtomPool::pool->isotopeMassNumber.find( symbol );
    if( it == ElementAtomPool::pool->isotopeMassNumber.end() )
      return 0;
    return it->second.size();
  }

  const Atom &Element::getIsotope( unsigned int n, IndexOrMassNumber nType ) const
  {
    if( Gbioinfo::gbioinfoDebug ){
      std::cerr << "\nEnter Element::getIsotope ..." << std::endl;
    }

    const std::vector<unsigned int> &vec =
      ElementAtomPool::pool->isotopeMassNumber.find( symbol )->second;
    unsigned int massNumber;
    switch( nType ){
    case IsotopeMassNumber :
      massNumber = n; break;
    case IsotopeIndex :
      if( n < 0 || n >= vec.size() )
        throw ElementAtomError( "isotope index out of range" ); // throw an exception
      massNumber = vec[ n ]; break;
    default:
      throw ElementAtomError( "it is impossible to be here" ); // never be here
      break;
    }

    const std::map<unsigned int, Atom*> &tmpMap =
      ElementAtomPool::instance()->isotopes.find( symbol )->second;
    std::map<unsigned int, Atom*>::const_iterator it = tmpMap.find( massNumber );
    if( it == tmpMap.end() ){
      std::stringstream ss( "there is no such atom: " );
      ss << symbol << massNumber << "(mass number).";
      throw ElementAtomError( ss.str() ); // throw an exception
    }
    return *( it->second );
  }

  double Element::getMonoMass() const
  {
    return ElementAtomPool::instance()->monoAverageMass.find( symbol )->second.first;
  }

  double Element::getAverageMass() const
  {
    return ElementAtomPool::instance()->monoAverageMass.find( symbol )->second.second;
  }

  void ElementAtomPool::initializeElements()
  {
    if( Gbioinfo::gbioinfoDebug )
      std::cerr << "\nEnter ElementAtomPool::initializeElements ..." << std::endl;

    elements.insert( new Element( 1,   "H",    "hydrogen"         ) );
    elements.insert( new Element( 2,   "He",   "Helium"           ) );
    elements.insert( new Element( 3,   "Li",   "Lithium"          ) );
    elements.insert( new Element( 4,   "Be",   "Beryllium"        ) );
    elements.insert( new Element( 5,   "B",    "Boron"            ) );
    elements.insert( new Element( 6,   "C",    "Carbon"           ) );
    elements.insert( new Element( 7,   "N",    "Nitrogen"         ) );
    elements.insert( new Element( 8,   "O",    "Oxygen"           ) );
    elements.insert( new Element( 9,   "F",    "Fluorine"         ) );
    elements.insert( new Element( 10,  "Ne",   "Neon"             ) );
    elements.insert( new Element( 11,  "Na",   "Sodium"           ) );
    elements.insert( new Element( 12,  "Mg",   "Magnesium"        ) );
    elements.insert( new Element( 13,  "Al",   "Aluminium"        ) );
    elements.insert( new Element( 14,  "Si",   "Silicon"          ) );
    elements.insert( new Element( 15,  "P",    "Phosphorus"       ) );
    elements.insert( new Element( 16,  "S",    "Sulfur"           ) );
    elements.insert( new Element( 17,  "Cl",   "Chlorine"         ) );
    elements.insert( new Element( 18,  "Ar",   "Argon"            ) );
    elements.insert( new Element( 19,  "K",    "Potassium"        ) );
    elements.insert( new Element( 20,  "Ca",   "Calcium"          ) );
    elements.insert( new Element( 21,  "Sc",   "Scandium"         ) );
    elements.insert( new Element( 22,  "Ti",   "Titanium"         ) );
    elements.insert( new Element( 23,  "V",    "Vanadium"         ) );
    elements.insert( new Element( 24,  "Cr",   "Chromium"         ) );
    elements.insert( new Element( 25,  "Mn",   "Manganese"        ) );
    elements.insert( new Element( 26,  "Fe",   "Iron"             ) );
    elements.insert( new Element( 27,  "Co",   "Cobalt"           ) );
    elements.insert( new Element( 28,  "Ni",   "Nickel"           ) );
    elements.insert( new Element( 29,  "Cu",   "Copper"           ) );
    elements.insert( new Element( 30,  "Zn",   "Zinc"             ) );
    elements.insert( new Element( 31,  "Ga",   "Gallium"          ) );
    elements.insert( new Element( 32,  "Ge",   "Germanium"        ) );
    elements.insert( new Element( 33,  "As",   "Arsenic"          ) );
    elements.insert( new Element( 34,  "Se",   "Selenium"         ) );
    elements.insert( new Element( 35,  "Br",   "Bromine"          ) );
    elements.insert( new Element( 36,  "Kr",   "Krypton"          ) );
    elements.insert( new Element( 37,  "Rb",   "Rubidium"         ) );
    elements.insert( new Element( 38,  "Sr",   "Strontium"        ) );
    elements.insert( new Element( 39,  "Y",    "Yttrium"          ) );
    elements.insert( new Element( 40,  "Zr",   "Zirconium"        ) );
    elements.insert( new Element( 41,  "Nb",   "Niobium"          ) );
    elements.insert( new Element( 42,  "Mo",   "Molybdenum"       ) );
    elements.insert( new Element( 43,  "Tc",   "Technetium"       ) );
    elements.insert( new Element( 44,  "Ru",   "Ruthenium"        ) );
    elements.insert( new Element( 45,  "Rh",   "Rhodium"          ) );
    elements.insert( new Element( 46,  "Pd",   "Palladium"        ) );
    elements.insert( new Element( 47,  "Ag",   "Silver"           ) );
    elements.insert( new Element( 48,  "Cd",   "Cadmium"          ) );
    elements.insert( new Element( 49,  "In",   "Indium"           ) );
    elements.insert( new Element( 50,  "Sn",   "Tin"              ) );
    elements.insert( new Element( 51,  "Sb",   "Antimony"         ) );
    elements.insert( new Element( 52,  "Te",   "Tellurium"        ) );
    elements.insert( new Element( 53,  "I",    "Iodine"           ) );
    elements.insert( new Element( 54,  "Xe",   "Xenon"            ) );
    elements.insert( new Element( 55,  "Cs",   "Caesium"          ) );
    elements.insert( new Element( 56,  "Ba",   "Barium"           ) );
    elements.insert( new Element( 57,  "La",   "Lanthanum"        ) );
    elements.insert( new Element( 58,  "Ce",   "Cerium"           ) );
    elements.insert( new Element( 59,  "Pr",   "Praseodymium"     ) );
    elements.insert( new Element( 60,  "Nd",   "Neodymium"        ) );
    elements.insert( new Element( 61,  "Pm",   "Promethium"       ) );
    elements.insert( new Element( 62,  "Sm",   "Samarium"         ) );
    elements.insert( new Element( 63,  "Eu",   "Europium"         ) );
    elements.insert( new Element( 64,  "Gd",   "Gadolinium"       ) );
    elements.insert( new Element( 65,  "Tb",   "Terbium"          ) );
    elements.insert( new Element( 66,  "Dy",   "Dysprosium"       ) );
    elements.insert( new Element( 67,  "Ho",   "Holmium"          ) );
    elements.insert( new Element( 68,  "Er",   "Erbium"           ) );
    elements.insert( new Element( 69,  "Tm",   "Thulium"          ) );
    elements.insert( new Element( 70,  "Yb",   "Ytterbium"        ) );
    elements.insert( new Element( 71,  "Lu",   "Lutetium"         ) );
    elements.insert( new Element( 72,  "Hf",   "Hafnium"          ) );
    elements.insert( new Element( 73,  "Ta",   "Tantalum"         ) );
    elements.insert( new Element( 74,  "W",    "Tungsten"         ) );
    elements.insert( new Element( 75,  "Re",   "Rhenium"          ) );
    elements.insert( new Element( 76,  "Os",   "Osmium"           ) );
    elements.insert( new Element( 77,  "Ir",   "Iridium"          ) );
    elements.insert( new Element( 78,  "Pt",   "Platinum"         ) );
    elements.insert( new Element( 79,  "Au",   "Gold"             ) );
    elements.insert( new Element( 80,  "Hg",   "Mercury"          ) );
    elements.insert( new Element( 81,  "Tl",   "Thallium"         ) );
    elements.insert( new Element( 82,  "Pb",   "Lead"             ) );
    elements.insert( new Element( 83,  "Bi",   "Bismuth"          ) );
    elements.insert( new Element( 84,  "Po",   "Polonium"         ) );
    elements.insert( new Element( 85,  "At",   "Astatine"         ) );
    elements.insert( new Element( 86,  "Rn",   "Radon"            ) );
    elements.insert( new Element( 87,  "Fr",   "Francium"         ) );
    elements.insert( new Element( 88,  "Ra",   "Radium"           ) );
    elements.insert( new Element( 89,  "Ac",   "Actinium"         ) );
    elements.insert( new Element( 90,  "Th",   "Thorium"          ) );
    elements.insert( new Element( 91,  "Pa",   "Protactinium"     ) );
    elements.insert( new Element( 92,  "U",    "Uranium"          ) );
    elements.insert( new Element( 93,  "Np",   "Neptunium"        ) );
    elements.insert( new Element( 94,  "Pu",   "Plutonium"        ) );
    elements.insert( new Element( 95,  "Am",   "Americium"        ) );
    elements.insert( new Element( 96,  "Cm",   "Curium"           ) );
    elements.insert( new Element( 97,  "Bk",   "Berkelium"        ) );
    elements.insert( new Element( 98,  "Cf",   "Californium"      ) );
    elements.insert( new Element( 99,  "Es",   "Einsteinium"      ) );
    elements.insert( new Element( 100, "Fm",   "Fermium"          ) );
    elements.insert( new Element( 101, "Md",   "Mendelevium"      ) );
    elements.insert( new Element( 102, "No",   "Nobelium"         ) );
    elements.insert( new Element( 103, "Lr",   "Lawrencium"       ) );
    elements.insert( new Element( 104, "Rf",   "Rutherfordium"    ) );
    elements.insert( new Element( 105, "Db",   "Dubnium"          ) );
    elements.insert( new Element( 106, "Sg",   "Seaborgium"       ) );
    elements.insert( new Element( 107, "Bh",   "Bohrium"          ) );
    elements.insert( new Element( 108, "Hs",   "Hassium"          ) );
    elements.insert( new Element( 109, "Mt",   "Meitnerium"       ) );
    elements.insert( new Element( 110, "Ds",   "Darmstadtium"     ) );
    elements.insert( new Element( 111, "Rg",   "Roentgenium"      ) );
    elements.insert( new Element( 112, "Uub",  "Ununbium"         ) );
    elements.insert( new Element( 113, "Uut",  "Ununtrium"        ) );
    elements.insert( new Element( 114, "Uuq",  "Ununquadium"      ) );
    elements.insert( new Element( 115, "Uup",  "Ununpentium"      ) );
    elements.insert( new Element( 116, "Uuh",  "Ununhexium"       ) );
    elements.insert( new Element( 117, "Uus",  "Ununseptium"      ) );
    elements.insert( new Element( 118, "Uuo",  "Ununoctium"       ) );

    for( std::set<Element*>::const_iterator it = elements.begin();
         it != elements.end(); ++it ){
      std::map<unsigned int, Element*>::iterator it2 =
	elementsByAtomicNumber.find( (*it)->atomicNumber );
      if( it2 != elementsByAtomicNumber.end() ){
	std::stringstream ss( "there are repulicated element records: " );
	ss << (*it)->symbol << "\t" << (*it)->atomicNumber;
	throw ElementAtomError( ss.str() );
      }
      elementsByAtomicNumber[ (*it)->atomicNumber ] = *it;

      std::map<std::string, Element*>::iterator it3 =
	elementsBySymbol.find( (*it)->symbol );
      if( it3 != elementsBySymbol.end() ){
	std::stringstream ss( "there are repulicated element records: " );
	ss << (*it)->symbol << "\t" << (*it)->atomicNumber;
	throw ElementAtomError( ss.str() );
      }
      elementsBySymbol[ (*it)->symbol ] = *it;
    }
    if( Gbioinfo::gbioinfoDebug )
      std::cerr << "\nLeave ElementAtomPool::initializeElements ..." << std::endl;
  }

  void ElementAtomPool::loadAtoms()
  {
    for( std::set<Atom*>::iterator it = atoms.begin();
         it != atoms.end(); ++it )
      delete (*it);

    atoms.clear();

    // Hydrogen
    //atoms.push_back( Atom( 1,  0,  1.0078250321, 0.99985 ) );
    //atoms.push_back( Atom( 1,  1,  2.014101778, 0.00015 ) );

    // Carbon
    //atoms.push_back( Atom( 6,  6, 12., 0.9998 ) );
    //atoms.push_back( Atom( 6,  7, 13.0033548378, 0.0002 ) );

    // Nitrogen
    //atoms.push_back( Atom( 7,  7, 14.0030740052, 0.9997 ) );
    //atoms.push_back( Atom( 7,  8, 15.0001088984, 0.0003 ) );

    // Oxygen
    //atoms.push_back( Atom( 8,  8, 15.9949146221, 0.99757 ) );
    //atoms.push_back( Atom( 8,  9, 16.9991315, 0.00038 ) );
    //atoms.push_back( Atom( 8,  10, 17.9991604, 0.00205 ) );

    // Phosphorus
    //atoms.push_back( Atom( 15, 16, 30.973761, 1.0 ) );

    // Sulfur
    //atoms.push_back( Atom( 16, 16, 31.97207069, 0.9493 ) );
    //atoms.push_back( Atom( 16, 17, 32.97145850, 0.0076 ) );
    //atoms.push_back( Atom( 16, 18, 33.96786683, 0.0429 ) );
    //atoms.push_back( Atom( 16, 20, 35.96708088, 0.0002 ) );

    // Cupper
    //atoms.push_back( Atom( 29, 63, 62.9296011, 0.6917 ) );
    //atoms.push_back( Atom( 29, 65, 64.9277937, 0.3083 ) );

    // Zinc
    //atoms.push_back( Atom( 30, 34, 63.9291466, 0.4863 ) );
    //atoms.push_back( Atom( 30, 36, 65.9260368, 0.279  ) );
    //atoms.push_back( Atom( 30, 37, 66.9271309, 0.041  ) );
    //atoms.push_back( Atom( 30, 38, 67.9248476, 0.1875 ) );
    //atoms.push_back( Atom( 30, 40, 69.925325, 0.0062  ) ); 

    atoms.insert( new Atom( 1, 0, 1.0078250321, 0.99985 ) );
    atoms.insert( new Atom( 1, 1, 2.0141017780, 0.00015 ) );
    atoms.insert( new Atom( 2, 1, 3.0160293097, 0.00000137 ) );
    atoms.insert( new Atom( 2, 2, 4.0026032497, 0.99999863 ) );
    atoms.insert( new Atom( 3, 3, 6.0151223, 0.0759 ) );
    atoms.insert( new Atom( 3, 4, 7.0160040, 0.9241 ) );
    atoms.insert( new Atom( 4, 5, 9.0121821, 1.0 ) );
    atoms.insert( new Atom( 5, 5, 10.0129370, 0.199 ) );
    atoms.insert( new Atom( 5, 6, 11.0093055, 0.801 ) );
    atoms.insert( new Atom( 6, 6, 12.0000000, 0.9893 ) );
    atoms.insert( new Atom( 6, 7, 13.0033548378, 0.0107 ) );
    atoms.insert( new Atom( 7, 7, 14.0030740052, 0.99632 ) );
    atoms.insert( new Atom( 7, 8, 15.0001088984, 0.00368 ) );
    atoms.insert( new Atom( 8, 8, 15.9949146221, 0.99757 ) );
    atoms.insert( new Atom( 8, 9, 16.99913150, 0.00038 ) );
    atoms.insert( new Atom( 8, 10, 17.9991604, 0.00205 ) );
    atoms.insert( new Atom( 9, 10, 18.99840320, 1.00 ) );
    atoms.insert( new Atom( 10, 10, 19.9924401759, 0.9048 ) );
    atoms.insert( new Atom( 10, 11, 20.99384674, 0.0027 ) );
    atoms.insert( new Atom( 10, 12, 21.99138551, 0.0925 ) );
    atoms.insert( new Atom( 11, 12, 22.98976967, 1. ) );
    atoms.insert( new Atom( 12, 12, 23.98504190, 0.7899 ) );
    atoms.insert( new Atom( 12, 13, 24.98583702, 0.1000 ) );
    atoms.insert( new Atom( 12, 14, 25.98259304, 0.1101 ) );
    atoms.insert( new Atom( 13, 14, 26.98153844, 1. ) );
    atoms.insert( new Atom( 14, 14, 27.9769265327, 0.922297 ) );
    atoms.insert( new Atom( 14, 15, 28.97649472, 0.046832 ) );
    atoms.insert( new Atom( 14, 16, 29.97377022, 0.030872 ) );
    atoms.insert( new Atom( 15, 16, 30.97376151, 1. ) );
    atoms.insert( new Atom( 16, 16, 31.97207069, 0.9493 ) );
    atoms.insert( new Atom( 16, 17, 32.97145850, 0.0076 ) );
    atoms.insert( new Atom( 16, 18, 33.96786683, 0.0429 ) );
    atoms.insert( new Atom( 16, 20, 35.96708088, 0.0002 ) );
    atoms.insert( new Atom( 17, 18, 34.96885271, 0.7578 ) );
    atoms.insert( new Atom( 17, 20, 36.96590260, 0.2422 ) );
    atoms.insert( new Atom( 18, 18, 35.96754628, 0.003365 ) );
    atoms.insert( new Atom( 18, 20, 37.9627322, 0.000632 ) );
    atoms.insert( new Atom( 18, 22, 39.962383123, 0.996003 ) );
    atoms.insert( new Atom( 19, 20, 38.9637069, 0.932581 ) );
    atoms.insert( new Atom( 19, 21, 39.96399867, 0.000117 ) );
    atoms.insert( new Atom( 19, 22, 40.96182597, 0.067302 ) );
    atoms.insert( new Atom( 20, 20, 39.9625912, 0.96941 ) );
    atoms.insert( new Atom( 20, 22, 41.9586183, 0.00647 ) );
    atoms.insert( new Atom( 20, 23, 42.9587668, 0.00135 ) );
    atoms.insert( new Atom( 20, 24, 43.9554811, 0.02086 ) );
    atoms.insert( new Atom( 20, 26, 45.9536928, 0.00004 ) );
    atoms.insert( new Atom( 20, 28, 47.952534, 0.00187 ) );
    atoms.insert( new Atom( 21, 24, 44.9559102, 1. ) );
    atoms.insert( new Atom( 22, 24, 45.9526295, 0.0825 ) );
    atoms.insert( new Atom( 22, 25, 46.9517638, 0.0744 ) );
    atoms.insert( new Atom( 22, 26, 47.9479471, 0.7372 ) );
    atoms.insert( new Atom( 22, 27, 48.9478708, 0.0541 ) );
    atoms.insert( new Atom( 22, 28, 49.9447921, 0.0518 ) );
    atoms.insert( new Atom( 23, 27, 49.9471628, 0.00250 ) );
    atoms.insert( new Atom( 23, 28, 50.9439637, 0.99750 ) );
    atoms.insert( new Atom( 24, 26, 49.9460496, 0.04345 ) );
    atoms.insert( new Atom( 24, 28, 51.9405119, 0.83789 ) );
    atoms.insert( new Atom( 24, 29, 52.9406538, 0.09501 ) );
    atoms.insert( new Atom( 24, 30, 53.9388849, 0.02365 ) );
    atoms.insert( new Atom( 25, 30, 54.9380496, 1. ) );
    atoms.insert( new Atom( 26, 28, 53.9396148, 0.05845 ) );
    atoms.insert( new Atom( 26, 30, 55.9349421, 0.91754 ) );
    atoms.insert( new Atom( 26, 31, 56.9353987, 0.02119 ) );
    atoms.insert( new Atom( 26, 32, 57.9332805, 0.00282 ) );
    atoms.insert( new Atom( 27, 32, 58.9332002, 1. ) );
    atoms.insert( new Atom( 28, 30, 57.9353479, 0.680769 ) );
    atoms.insert( new Atom( 28, 32, 59.9307906, 0.262231 ) );
    atoms.insert( new Atom( 28, 33, 60.9310604, 0.011399 ) );
    atoms.insert( new Atom( 28, 34, 61.9283488, 0.036345 ) );
    atoms.insert( new Atom( 28, 36, 63.9279696, 0.009256 ) );
    atoms.insert( new Atom( 29, 34, 62.9296011, 0.6917 ) );
    atoms.insert( new Atom( 29, 36, 64.9277937, 0.3083 ) );
    atoms.insert( new Atom( 30, 34, 63.9291466, 0.4863 ) );
    atoms.insert( new Atom( 30, 36, 65.9260368, 0.2790 ) );
    atoms.insert( new Atom( 30, 37, 66.9271309, 0.0410 ) );
    atoms.insert( new Atom( 30, 38, 67.9248476, 0.1875 ) );
    atoms.insert( new Atom( 30, 40, 69.925325, 0.0062 ) );
    atoms.insert( new Atom( 31, 38, 68.925581, 0.60108 ) );
    atoms.insert( new Atom( 31, 40, 70.9247050, 0.39892 ) );
    atoms.insert( new Atom( 32, 38, 69.9242504, 0.2084 ) );
    atoms.insert( new Atom( 32, 40, 71.9220762, 0.2754 ) );
    atoms.insert( new Atom( 32, 41, 72.9234594, 0.0773 ) );
    atoms.insert( new Atom( 32, 42, 73.9211782, 0.3628 ) );
    atoms.insert( new Atom( 32, 44, 75.9214027, 0.0761 ) );
    atoms.insert( new Atom( 33, 42, 74.9215964, 1. ) );
    atoms.insert( new Atom( 34, 40, 73.9224766, 0.0089 ) );
    atoms.insert( new Atom( 34, 42, 75.9192141, 0.0937 ) );
    atoms.insert( new Atom( 34, 43, 76.9199146, 0.0763 ) );
    atoms.insert( new Atom( 34, 44, 77.9173095, 0.2377 ) );
    atoms.insert( new Atom( 34, 46, 79.9165218, 0.4961 ) );
    atoms.insert( new Atom( 34, 48, 81.9167000, 0.0873 ) );
    atoms.insert( new Atom( 35, 44, 78.9183376, 0.5069 ) );
    atoms.insert( new Atom( 35, 46, 80.916291, 0.4931 ) );
    atoms.insert( new Atom( 36, 42, 77.920386, 0.0035 ) );
    atoms.insert( new Atom( 36, 44, 79.916378, 0.028 ) );
    atoms.insert( new Atom( 36, 46, 81.9134846, 0.1158 ) );
    atoms.insert( new Atom( 36, 47, 82.914136, 0.1149 ) );
    atoms.insert( new Atom( 36, 48, 83.911507, 0.5700 ) );
    atoms.insert( new Atom( 36, 50, 85.9106103, 0.1730 ) );
    atoms.insert( new Atom( 37, 48, 84.9117893, 0.7217 ) );
    atoms.insert( new Atom( 37, 50, 86.9091835, 0.2783 ) );
    atoms.insert( new Atom( 38, 46, 83.913425, 0.0056 ) );
    atoms.insert( new Atom( 38, 48, 85.9092624, 0.0986 ) );
    atoms.insert( new Atom( 38, 49, 86.9088793, 0.0700 ) );
    atoms.insert( new Atom( 38, 50, 87.9056143, 0.8258 ) );
    atoms.insert( new Atom( 39, 50, 88.9058479, 1. ) );
    atoms.insert( new Atom( 40, 50, 89.9047037, 0.5145 ) );
    atoms.insert( new Atom( 40, 51, 90.9056450, 0.1122 ) );
    atoms.insert( new Atom( 40, 52, 91.9050401, 0.1715 ) );
    atoms.insert( new Atom( 40, 54, 93.9063158, 0.1738 ) );
    atoms.insert( new Atom( 40, 56, 95.908276, 0.0280 ) );
    atoms.insert( new Atom( 41, 52, 92.9063775, 1. ) );
    atoms.insert( new Atom( 42, 50, 91.906810, 0.1484 ) );
    atoms.insert( new Atom( 42, 52, 93.9050876, 0.0925 ) );
    atoms.insert( new Atom( 42, 53, 94.9058415, 0.1592 ) );
    atoms.insert( new Atom( 42, 54, 95.9046789, 0.1668 ) );
    atoms.insert( new Atom( 42, 55, 96.9060210, 0.0955 ) );
    atoms.insert( new Atom( 42, 56, 97.9054078, 0.2413 ) );
    atoms.insert( new Atom( 42, 68, 99.907477, 0.0963 ) );
    atoms.insert( new Atom( 43, 54, 96.906365, 0. ) );
    atoms.insert( new Atom( 43, 55, 97.907216, 0. ) );
    atoms.insert( new Atom( 43, 56, 98.9062546, 0. ) );
    atoms.insert( new Atom( 44, 52, 95.907598, 0.0554 ) );
    atoms.insert( new Atom( 44, 54, 97.905287, 0.0187 ) );
    atoms.insert( new Atom( 44, 55, 98.9059393, 0.1276 ) );
    atoms.insert( new Atom( 44, 56, 99.9042197, 0.1260 ) );
    atoms.insert( new Atom( 44, 57, 100.9055822, 0.1706 ) );
    atoms.insert( new Atom( 44, 58, 101.9043495, 0.3155 ) );
    atoms.insert( new Atom( 44, 60, 103.905430, 0.1862 ) );
    atoms.insert( new Atom( 45, 58, 102.905504, 1. ) );
    atoms.insert( new Atom( 46, 56, 101.905608, 0.0102 ) );
    atoms.insert( new Atom( 46, 58, 103.904035, 0.1114 ) );
    atoms.insert( new Atom( 46, 59, 104.905084, 0.2233 ) );
    atoms.insert( new Atom( 46, 60, 105.903483, 0.2733 ) );
    atoms.insert( new Atom( 46, 62, 107.903894, 0.2646 ) );
    atoms.insert( new Atom( 46, 64, 109.905152, 0.1172 ) );
    atoms.insert( new Atom( 47, 60, 106.905093, 0.51839 ) );
    atoms.insert( new Atom( 47, 62, 108.904756, 0.48161 ) );
    atoms.insert( new Atom( 48, 58, 105.906458, 0.0125 ) );
    atoms.insert( new Atom( 48, 60, 107.904183, 0.0089 ) );
    atoms.insert( new Atom( 48, 62, 109.903006, 0.1249 ) );
    atoms.insert( new Atom( 48, 63, 110.904182, 0.1280 ) );
    atoms.insert( new Atom( 48, 64, 111.9027572, 0.2413 ) );
    atoms.insert( new Atom( 48, 65, 112.9044009, 0.1222 ) );
    atoms.insert( new Atom( 48, 66, 113.9033581, 0.2873 ) );
    atoms.insert( new Atom( 48, 68, 115.904755, 0.0749 ) );
    atoms.insert( new Atom( 49, 64, 112.904061, 0.0429 ) );
    atoms.insert( new Atom( 49, 66, 114.903878, 0.9571 ) );
    atoms.insert( new Atom( 50, 62, 111.904821, 0.0097 ) );
    atoms.insert( new Atom( 50, 64, 113.902782, 0.0066 ) );
    atoms.insert( new Atom( 50, 65, 114.903346, 0.0034 ) );
    atoms.insert( new Atom( 50, 66, 115.901744, 0.1454 ) );
    atoms.insert( new Atom( 50, 67, 116.902954, 0.0768 ) );
    atoms.insert( new Atom( 50, 68, 117.901606, 0.2422 ) );
    atoms.insert( new Atom( 50, 69, 118.903309, 0.0859 ) );
    atoms.insert( new Atom( 50, 70, 119.9021966, 0.3258 ) );
    atoms.insert( new Atom( 50, 72, 121.9034401, 0.0463 ) );
    atoms.insert( new Atom( 50, 74, 123.9052746, 0.0579 ) );
    atoms.insert( new Atom( 51, 60, 120.9038180, 0.5721 ) );
    atoms.insert( new Atom( 51, 62, 122.9042157, 0.4279 ) );
    atoms.insert( new Atom( 52, 68, 119.904020, 0.0009 ) );
    atoms.insert( new Atom( 52, 70, 121.9030471, 0.0255 ) );
    atoms.insert( new Atom( 52, 71, 122.9042730, 0.0089 ) );
    atoms.insert( new Atom( 52, 72, 123.9028195, 0.0474 ) );
    atoms.insert( new Atom( 52, 73, 124.9044247, 0.0707 ) );
    atoms.insert( new Atom( 52, 74, 125.9033055, 0.1884 ) );
    atoms.insert( new Atom( 52, 76, 127.9044614, 0.3174 ) );
    atoms.insert( new Atom( 52, 78, 129.9062228, 0.3408 ) );
    atoms.insert( new Atom( 53, 74, 126.904468, 1. ) );
    atoms.insert( new Atom( 54, 70, 123.9058958, 0.0009 ) );
    atoms.insert( new Atom( 54, 72, 125.904269, 0.0009 ) );
    atoms.insert( new Atom( 54, 74, 127.9035304, 0.0192 ) );
    atoms.insert( new Atom( 54, 75, 128.9047795, 0.2644 ) );
    atoms.insert( new Atom( 54, 76, 129.9035079, 0.0408 ) );
    atoms.insert( new Atom( 54, 77, 130.9050819, 0.2118 ) );
    atoms.insert( new Atom( 54, 78, 131.9041545, 0.2689 ) );
    atoms.insert( new Atom( 54, 80, 133.9053945, 0.1044 ) );
    atoms.insert( new Atom( 54, 82, 135.907220, 0.0887 ) );
    atoms.insert( new Atom( 55, 78, 132.905447, 1.00 ) );
    atoms.insert( new Atom( 56, 74, 129.906310, 0.00106 ) );
    atoms.insert( new Atom( 56, 76, 131.905056, 0.00101 ) );
    atoms.insert( new Atom( 56, 78, 133.904503, 0.02417 ) );
    atoms.insert( new Atom( 56, 79, 134.905683, 0.06592 ) );
    atoms.insert( new Atom( 56, 80, 135.904570, 0.07854 ) );
    atoms.insert( new Atom( 56, 81, 136.905821, 0.11232 ) );
    atoms.insert( new Atom( 56, 82, 137.905241, 0.71698 ) );
    atoms.insert( new Atom( 57, 81, 137.907107, 0.00090 ) );
    atoms.insert( new Atom( 57, 82, 138.906348, 0.99910 ) );
    atoms.insert( new Atom( 58, 78, 135.907140, 0.00185 ) );
    atoms.insert( new Atom( 58, 80, 137.905986, 0.00251 ) );
    atoms.insert( new Atom( 58, 82, 139.905434, 0.88450 ) );
    atoms.insert( new Atom( 58, 84, 141.909240, 0.11114 ) );
    atoms.insert( new Atom( 59, 82, 140.907648, 1. ) );
    atoms.insert( new Atom( 60, 82, 141.907719, 0.272 ) );
    atoms.insert( new Atom( 60, 83, 142.909810, 0.122 ) );
    atoms.insert( new Atom( 60, 84, 143.910083, 0.238 ) );
    atoms.insert( new Atom( 60, 85, 144.912569, 0.083 ) );
    atoms.insert( new Atom( 60, 86, 145.913112, 0.172 ) );
    atoms.insert( new Atom( 60, 88, 147.916889, 0.057 ) );
    atoms.insert( new Atom( 60, 90, 149.920887, 0.056 ) );
    atoms.insert( new Atom( 61, 84, 144.912744, 0. ) );
    atoms.insert( new Atom( 61, 86, 146.915134, 0. ) );
    atoms.insert( new Atom( 62, 82, 143.911995, 0.0307 ) );
    atoms.insert( new Atom( 62, 85, 146.914893, 0.1499 ) );
    atoms.insert( new Atom( 62, 86, 147.914818, 0.1124 ) );
    atoms.insert( new Atom( 62, 87, 148.917180, 0.1382 ) );
    atoms.insert( new Atom( 62, 88, 149.917271, 0.0738 ) );
    atoms.insert( new Atom( 62, 90, 151.919728, 0.2675 ) );
    atoms.insert( new Atom( 62, 92, 153.922205, 0.2275 ) );
    atoms.insert( new Atom( 63, 88, 150.919846, 0.4781 ) );
    atoms.insert( new Atom( 63, 90, 152.921226, 0.5219 ) );
    atoms.insert( new Atom( 64, 88, 151.919788, 0.0020 ) );
    atoms.insert( new Atom( 64, 90, 153.920862, 0.0218 ) );
    atoms.insert( new Atom( 64, 91, 154.922619, 0.1480 ) );
    atoms.insert( new Atom( 64, 92, 155.922120, 0.2047 ) );
    atoms.insert( new Atom( 64, 93, 156.923957, 0.1565 ) );
    atoms.insert( new Atom( 64, 94, 157.924101, 0.2484 ) );
    atoms.insert( new Atom( 64, 96, 159.927051, 0.2186 ) );
    atoms.insert( new Atom( 65, 94, 158.925343, 1. ) );
    atoms.insert( new Atom( 66, 90, 155.924278, 0.0006 ) );
    atoms.insert( new Atom( 66, 92, 157.924405, 0.0010 ) );
    atoms.insert( new Atom( 66, 94, 159.925194, 0.0234 ) );
    atoms.insert( new Atom( 66, 95, 160.926930, 0.1891 ) );
    atoms.insert( new Atom( 66, 96, 161.926795, 0.2551 ) );
    atoms.insert( new Atom( 66, 97, 162.928728, 0.2490 ) );
    atoms.insert( new Atom( 66, 98, 163.929171, 0.2818 ) );
    atoms.insert( new Atom( 67, 98, 164.930319, 1. ) );
    atoms.insert( new Atom( 68, 94, 161.928775, 0.0014 ) );
    atoms.insert( new Atom( 68, 96, 163.929197, 0.0161 ) );
    atoms.insert( new Atom( 68, 98, 165.930290, 0.3361 ) );
    atoms.insert( new Atom( 68, 99, 166.932045, 0.2293 ) );
    atoms.insert( new Atom( 68, 100, 167.932368, 0.2678 ) );
    atoms.insert( new Atom( 68, 102, 169.935460, 0.1493 ) );
    atoms.insert( new Atom( 69, 100, 168.934211, 1. ) );
    atoms.insert( new Atom( 70, 98, 167.933894, 0.0013 ) );
    atoms.insert( new Atom( 70, 100, 169.934759, 0.0304 ) );
    atoms.insert( new Atom( 70, 101, 170.936322, 0.1428 ) );
    atoms.insert( new Atom( 70, 102, 171.9363777, 0.2183 ) );
    atoms.insert( new Atom( 70, 103, 172.9382068, 0.1613 ) );
    atoms.insert( new Atom( 70, 104, 173.9388581, 0.3183 ) );
    atoms.insert( new Atom( 70, 106, 175.942568, 0.1276 ) );
    atoms.insert( new Atom( 71, 104, 174.9407679, 0.9741 ) );
    atoms.insert( new Atom( 71, 105, 175.9426824, 0.0259 ) );
    atoms.insert( new Atom( 72, 102, 173.940040, 0.0016 ) );
    atoms.insert( new Atom( 72, 104, 175.9414018, 0.0526 ) );
    atoms.insert( new Atom( 72, 105, 176.9432200, 0.1860 ) );
    atoms.insert( new Atom( 72, 106, 177.9436977, 0.2728 ) );
    atoms.insert( new Atom( 72, 107, 178.9458151, 0.1362 ) );
    atoms.insert( new Atom( 72, 108, 179.9465488, 0.3508 ) );
    atoms.insert( new Atom( 73, 107, 179.947466, 0.00012 ) );
    atoms.insert( new Atom( 73, 108, 180.947996, 0.99988 ) );
    atoms.insert( new Atom( 74, 106, 179.946706, 0.0012 ) );
    atoms.insert( new Atom( 74, 108, 181.948206, 0.2650 ) );
    atoms.insert( new Atom( 74, 109, 182.9502245, 0.1431 ) );
    atoms.insert( new Atom( 74, 110, 183.9509326, 0.3064 ) );
    atoms.insert( new Atom( 74, 112, 185.954362, 0.2843 ) );
    atoms.insert( new Atom( 75, 110, 184.9529557, 0.3740 ) );
    atoms.insert( new Atom( 75, 112, 186.9557508, 0.6260 ) );
    atoms.insert( new Atom( 76, 108, 183.952491, 0.0002 ) );
    atoms.insert( new Atom( 76, 110, 185.953838, 0.0159 ) );
    atoms.insert( new Atom( 76, 111, 186.9557479, 0.0196 ) );
    atoms.insert( new Atom( 76, 112, 187.9558360, 0.1324 ) );
    atoms.insert( new Atom( 76, 113, 188.9581449, 0.1615 ) );
    atoms.insert( new Atom( 76, 114, 189.958445, 0.2626 ) );
    atoms.insert( new Atom( 76, 116, 191.961479, 0.4078 ) );
    atoms.insert( new Atom( 77, 114, 190.960591, 0.373 ) );
    atoms.insert( new Atom( 77, 116, 192.962924, 0.627 ) );
    atoms.insert( new Atom( 78, 112, 189.959930, 0.00014 ) );
    atoms.insert( new Atom( 78, 114, 191.961035, 0.00782 ) );
    atoms.insert( new Atom( 78, 116, 193.962664, 0.32967 ) );
    atoms.insert( new Atom( 78, 117, 194.964774, 0.33832 ) );
    atoms.insert( new Atom( 78, 118, 195.964935, 0.25242 ) );
    atoms.insert( new Atom( 78, 120, 197.967876, 0.07163 ) );
    atoms.insert( new Atom( 79, 118, 196.966552, 1. ) );
    atoms.insert( new Atom( 80, 116, 195.965815, 0.0015 ) );
    atoms.insert( new Atom( 80, 118, 197.966752, 0.0997 ) );
    atoms.insert( new Atom( 80, 119, 198.968262, 0.1687 ) );
    atoms.insert( new Atom( 80, 120, 199.968309, 0.2310 ) );
    atoms.insert( new Atom( 80, 121, 200.970285, 0.1318 ) );
    atoms.insert( new Atom( 80, 122, 201.970626, 0.2986 ) );
    atoms.insert( new Atom( 80, 124, 203.973476, 0.0687 ) );
    atoms.insert( new Atom( 81, 122, 202.972329, 0.29524 ) );
    atoms.insert( new Atom( 81, 124, 204.974412, 0.70476 ) );
    atoms.insert( new Atom( 82, 122, 203.973029, 0.014 ) );
    atoms.insert( new Atom( 82, 124, 205.974449, 0.241 ) );
    atoms.insert( new Atom( 82, 125, 206.975881, 0.221 ) );
    atoms.insert( new Atom( 82, 126, 207.976636, 0.524 ) );
    atoms.insert( new Atom( 83, 126, 208.980383, 1. ) );
    atoms.insert( new Atom( 84, 125, 208.982416, 0. ) );
    atoms.insert( new Atom( 84, 126, 209.982857, 0. ) );
    atoms.insert( new Atom( 85, 125, 209.987131, 0. ) );
    atoms.insert( new Atom( 85, 126, 210.987481, 0. ) );
    atoms.insert( new Atom( 86, 125, 210.990585, 0. ) );
    atoms.insert( new Atom( 86, 134, 220.0113841, 0. ) );
    atoms.insert( new Atom( 86, 136, 222.0175705, 0. ) );
    atoms.insert( new Atom( 87, 135, 223.0197307, 0. ) );
    atoms.insert( new Atom( 88, 135, 223.018497, 0. ) );
    atoms.insert( new Atom( 88, 136, 224.0202020, 0. ) );
    atoms.insert( new Atom( 88, 138, 226.0254026, 0. ) );
    atoms.insert( new Atom( 88, 140, 228.0310641, 0. ) );
    atoms.insert( new Atom( 89, 138, 227.0277470, 0. ) );
    atoms.insert( new Atom( 90, 140, 230.0331266, 0. ) );
    atoms.insert( new Atom( 90, 142, 232.0380504, 1. ) );
    atoms.insert( new Atom( 91, 140, 231.0358789, 1. ) );
    atoms.insert( new Atom( 92, 141, 233.039628, 0. ) );
    atoms.insert( new Atom( 92, 142, 234.0409456, 0.000055 ) );
    atoms.insert( new Atom( 92, 143, 235.0439231, 0.007200 ) );
    atoms.insert( new Atom( 92, 144, 236.0455619, 0. ) );
    atoms.insert( new Atom( 92, 146, 238.0507826, 0.992745 ) );
    atoms.insert( new Atom( 93, 144, 237.0481673, 0. ) );
    atoms.insert( new Atom( 93, 146, 239.0529314, 0. ) );
    atoms.insert( new Atom( 94, 144, 238.0495534, 0. ) );
    atoms.insert( new Atom( 94, 145, 239.0521565, 0. ) );
    atoms.insert( new Atom( 94, 146, 240.0538075, 0. ) );
    atoms.insert( new Atom( 94, 147, 241.0568453, 0. ) );
    atoms.insert( new Atom( 94, 148, 242.0587368, 0. ) );
    atoms.insert( new Atom( 94, 150, 244.064198, 0. ) );
    atoms.insert( new Atom( 95, 146, 241.0568229, 0. ) );
    atoms.insert( new Atom( 95, 148, 243.0613727, 0. ) );
    atoms.insert( new Atom( 96, 147, 243.0613822, 0. ) );
    atoms.insert( new Atom( 96, 148, 244.0627463, 0. ) );
    atoms.insert( new Atom( 96, 149, 245.0654856, 0. ) );
    atoms.insert( new Atom( 96, 150, 246.0672176, 0. ) );
    atoms.insert( new Atom( 96, 151, 247.070347, 0. ) );
    atoms.insert( new Atom( 96, 152, 248.072342, 0. ) );
    atoms.insert( new Atom( 97, 150, 247.070299, 0. ) );
    atoms.insert( new Atom( 97, 152, 249.074980, 0. ) );
    atoms.insert( new Atom( 98, 151, 249.074847, 0. ) );
    atoms.insert( new Atom( 98, 152, 250.0764000, 0. ) );
    atoms.insert( new Atom( 98, 153, 251.079580, 0. ) );
    atoms.insert( new Atom( 98, 154, 252.081620, 0. ) );
    atoms.insert( new Atom( 99, 153, 252.082970, 0. ) );
    atoms.insert( new Atom( 100, 157, 257.095099, 0. ) );
    atoms.insert( new Atom( 101, 155, 256.094050, 0. ) );
    atoms.insert( new Atom( 101, 157, 258.098425, 0. ) );
    atoms.insert( new Atom( 102, 157, 259.10102, 0. ) );
    atoms.insert( new Atom( 103, 159, 262.10969, 0. ) );
    atoms.insert( new Atom( 104, 157, 261.10875, 0. ) );
    atoms.insert( new Atom( 105, 157, 262.11415, 0. ) );
    atoms.insert( new Atom( 106, 160, 266.12193, 0. ) );
    atoms.insert( new Atom( 107, 157, 264.12473, 0. ) );
    atoms.insert( new Atom( 108, 169, 0., 0. ) );
    atoms.insert( new Atom( 109, 159, 268.13882, 0. ) );
    atoms.insert( new Atom( 110, 171, 0., 0. ) );
    atoms.insert( new Atom( 111, 161, 272.15348, 0. ) );
    atoms.insert( new Atom( 112, 173, 0., 0. ) );
    atoms.insert( new Atom( 114, 175, 0., 0. ) );
    atoms.insert( new Atom( 116, 176, 0., 0. ) );

    calculateIsotope();
    calculateMonoAverageMass();
  }

  void ElementAtomPool::loadAtoms( const std::string &filename )
  {
    std::ifstream inFile( filename.c_str() );
    if( ! inFile ){
      throw ElementAtomError( "can not open atom file: " + filename );
    }

    for( std::set<Atom*>::iterator it = atoms.begin();
         it != atoms.end(); ++it )
      delete (*it);

    atoms.clear();
    unsigned int m, n;
    double x, y;
    while( inFile >> m >> n >> x >> y ){
      atoms.insert( new Atom( m, n, x, y ) );
    }

    calculateIsotope();
    calculateMonoAverageMass();
  }

  void ElementAtomPool::calculateIsotope()
  {
    isotopes.clear();
    isotopeMassNumber.clear();

    for( std::set<Atom*>::const_iterator it = atoms.begin();
         it != atoms.end(); ++it ){
      std::map<unsigned int, Element*>::const_iterator it2 =
	elementsByAtomicNumber.find( (*it)->protonNumber );
      if( it2 == elementsByAtomicNumber.end() ){
        std::stringstream ss( "atom p" );
        ss << (*it)->protonNumber << " n" << (*it)->neutronNumber << " does NOT exist";
        throw ElementAtomError( ss.str() );
      }
      unsigned int massNumber = (*it)->protonNumber + (*it)->neutronNumber;
      std::string symbol( it2->second->symbol );

      std::map<std::string, std::map<unsigned int, Atom*> >::iterator it3 =
	isotopes.find( symbol );
      if( it3 == isotopes.end() )
	isotopes[ symbol ][ massNumber ] = *it;
      else{
	std::map<unsigned int, Atom*>::iterator it4 = it3->second.find( massNumber );
	if( it4 != it3->second.end() ){
	  std::stringstream ss( "there are repulicated atom records: " );
	  ss << (*it)->protonNumber << "\t" << (*it)->neutronNumber;
	  throw ElementAtomError( ss.str() );
	}
	it3->second[ massNumber ] = *it;
      }
    }

    for( std::map<std::string, std::map<unsigned int, Atom*> >::const_iterator
	   it = isotopes.begin(); it != isotopes.end(); ++it ){
      isotopeMassNumber[ it->first ].resize( it->second.size() );
      std::map<unsigned int, Atom*>::const_iterator it2 = it->second.begin();
      std::map<unsigned int, Atom*>::size_type i = 0;
      for( ; it2 != it->second.end(); ++it2, ++i )
	isotopeMassNumber[ it->first ][ i ] = it2->first;
    }

    for( std::map<std::string, std::vector<unsigned int> >::iterator
           it = isotopeMassNumber.begin(); it != isotopeMassNumber.end(); ++it ){
      std::vector<unsigned int> &vec = it->second;
      std::sort( vec.begin(), vec.end() );
    }
  }

  void ElementAtomPool::calculateMonoAverageMass()
  {
    monoAverageMass.clear();

    for( std::map<std::string, std::map<unsigned int, Atom*> >::iterator
           it = isotopes.begin(); it != isotopes.end(); ++it ){
      //std::cerr << it->first << std::endl;
      double monoMass = 0., averageMass = 0.;
      double prob = 0.;
      std::map<unsigned int, Atom*> &atoms = it->second;
      for( std::map<unsigned int, Atom*>::iterator
             it2 = atoms.begin(); it2 != atoms.end(); ++it2 ){
        //std::cerr << "it2: " << it2->first << "\t" <<  it2->second->mass << "\t" << it2->second->abundance << std::endl;
        // mono mass
        if( it2->second->abundance > prob ){
          monoMass = it2->second->mass;
          prob = it2->second->abundance;
        }

        // average mass
        averageMass += it2->second->mass * it2->second->abundance;
      }
      //std::cerr << monoMass << "\t" << averageMass << std::endl;
      monoAverageMass[ it->first ] = std::pair<double, double>( monoMass, averageMass );
    }
  }

  double ElementAtomPool::getAtomMass
  ( const std::string &elementSymbol, const unsigned int massNumber )
  {
    return getElement( elementSymbol ).getIsotope( massNumber, Element::IsotopeMassNumber ).mass;
  }

  void ElementAtomPool::getAllAtomicNumber( std::vector<unsigned int> &vec )
  {
    vec.clear();
    vec.resize( elementsByAtomicNumber.size() );
    std::map<unsigned int, Element*>::const_iterator it = elementsByAtomicNumber.begin();
    std::map<unsigned int, Element*>::size_type i = 0;
    for( ; it != elementsByAtomicNumber.end(); ++it, ++i )
      vec[ i ] = it->first;
  }

  void ElementAtomPool::getAllElementSymbol( std::vector<std::string> &vec )
  {
    std::vector<unsigned int> atomicNumbers;
    getAllAtomicNumber( atomicNumbers );
    vec.clear();
    vec.resize( atomicNumbers.size() );
    for( std::vector<unsigned int>::size_type i = 0;
	 i != atomicNumbers.size(); ++i )
      vec[ i ] = elementsByAtomicNumber.find( atomicNumbers[ i ] )->second->symbol;
  }

  double ElementAtomPool::getMonoMass( const std::map<std::string, unsigned int> &formula )
  {
    double result = 0.;
    for( std::map<std::string, unsigned int>::const_iterator
           it = formula.begin(); it != formula.end(); ++it ){
      result += getElement( it->first ).getMonoMass();
    }
    return result;
  }

  double ElementAtomPool::getAverageMass( const std::map<std::string, unsigned int> &formula )
  {
    double result = 0.;
    for( std::map<std::string, unsigned int>::const_iterator
           it = formula.begin(); it != formula.end(); ++it ){
      result += getElement( it->first ).getAverageMass();
    }
    return result;
  }

  double ElementAtomPool::getMonoMass( const std::string& formula )
  {
    return getMonoMass( ChemicalFormula::string2map( formula ) );
  }

  double ElementAtomPool::getAverageMass( const std::string& formula )
  {
    return getAverageMass( ChemicalFormula::string2map( formula ) );
  }
  /*
  unsigned int ElementAtomPool::getNumberOfIsotopes( const std::string &elementSymbol )
  {
    const Element &element = ElementAtomPool::instance()->getElement( elementSymbol );
    
    return element.getNumberOfIsotopes();
  }
  */

  std::ostream& operator<<( std::ostream &os, const ElementAtomPool &eap )
  {
    std::vector<unsigned int> atomicNumbers( eap.elementsByAtomicNumber.size() );
    std::map<unsigned int, Element*>::const_iterator it =
      eap.elementsByAtomicNumber.begin();
    for( std::map<unsigned int, Element*>::size_type i = 0; it != eap.elementsByAtomicNumber.end(); ++it, ++i )
      atomicNumbers[ i ] = it->first;

    std::sort( atomicNumbers.begin(), atomicNumbers.end() );

    for( std::vector<unsigned int>::size_type i = 0; i != atomicNumbers.size(); ++i ){
      const Element& element = *( eap.elementsByAtomicNumber.find( atomicNumbers[ i ] )->second );
      unsigned int n = element.getNumberOfIsotopes();
      os << n << " isotopes" << std::endl;
      for( unsigned int j = 0; j != n; ++j ){
	const Atom &atom = element.getIsotope( j, Element::IsotopeIndex );
	os << element.atomicNumber << "\t" << element.symbol << "\t"
	   << atom.getMassNumber() << "\t" << atom.mass << "\t"
	   << atom.abundance;
	if( j == n )
	  continue;
	os << "\n";
      }
      if( i == atomicNumbers.size() )
	continue;
      os << "\n";
    }
    return os;
  }

  std::map<std::string, unsigned int> ChemicalFormula::string2map( const std::string &formula )
  {
    std::map<std::string, unsigned int> formulaMap;

    {
      // remove the spaces
      Glib::RefPtr<Glib::Regex> re = Glib::Regex::create( "\\s+" );
      Glib::ustring str = re->replace( Glib::ustring( formula ), 0, Glib::ustring( "" ),
                                       static_cast<Glib::RegexMatchFlags>(0) );
      //std::cout << "Remove spaces successfully! " << str << std::endl;

      // Add whitespace before uppercase
      Glib::RefPtr<Glib::Regex> re2 = Glib::Regex::create( "([[:upper:]])" );
      //std::cout << "Pattern is " << re2->get_pattern() << std::endl;
      str = re2->replace( str, 1, " \\1", static_cast<Glib::RegexMatchFlags>(0) );
      //std::cout << "Add whitespace before uppercase successfully! " << str << std::endl;

      // Split using white space as separator
      std::vector<Glib::ustring> elementNumber = Glib::Regex::split_simple( " ", str );
      //for( std::vector<Glib::ustring>::size_type i = 0; i < elementNumber.size(); ++i ){
      //  std::cout << elementNumber[ i ] << std::endl;
      //}

      // Get the element name and the number
      Glib::RefPtr<Glib::Regex> re3 = Glib::Regex::create( "^([[:upper:]][[:lower:]]{0,3})(\\d*)$" );
      for( std::vector<Glib::ustring>::size_type i = 0; i < elementNumber.size(); ++i ){
        if( re3->match( elementNumber[ i ] ) ){
          str = re3->replace( elementNumber[ i ], 0, "\\1 \\2", static_cast<Glib::RegexMatchFlags>(0) );
          std::vector<Glib::ustring> elementNumber2 = Glib::Regex::split_simple( " ", str );
          if( elementNumber2.size() == 2 && elementNumber2[ 1 ] != "" ){
            std::istringstream iss( elementNumber2[ 1 ] );
            unsigned int n;
            iss >> n;
            formulaMap[ elementNumber2[ 0 ] ] += n;
          }
          else{
            formulaMap[ elementNumber2[ 0 ] ] += 1;
          }
        }
        else{
          std::string message( "Case problem in formula, please check it." );
          displayMessage( message, Gtk::MESSAGE_ERROR );
          return formulaMap;
        }
      }
    }

    // boost::regex implementation
    //std::string::const_iterator start = formula.begin(), end = formula.end();
    //boost::match_results<std::string::const_iterator> results;
    //const boost::regex eBegin( "^[[:upper:]]" );
    //if( ! regex_search( start, end, results,
    //                    eBegin, boost::match_default ) ){
    //  std::string message( "first element (letter) is not correct" );
    //  displayMessage( message, Gtk::MESSAGE_ERROR );
    //  return formulaMap;
    //}
    //
    //const boost::regex
    //  e( "([[:upper:]][[:lower:]]?)[[:space:]]*([[:digit:]]*)" );
    //
    //while( regex_search( start, end, results, e, boost::match_default ) ){
    //  start = results[ 0 ].second;
    //  std::string element( results[ 1 ].first, results[ 1 ].second );
    //  std::string number( results[ 2 ].first, results[ 2 ].second );
    //
    //  unsigned int n = 1;
    //  if( ! number.empty() )
    //    n = boost::lexical_cast<unsigned int>( number );
    //
    //  formulaMap[ element ] += n;
    //}

    return formulaMap;
  }

  std::string ChemicalFormula::map2string( const std::map<std::string, unsigned int> &formulaMap, bool isMarkup )
  {
    std::ostringstream oss;
    for( std::map<std::string, unsigned int>::const_iterator
           it = formulaMap.begin(); it != formulaMap.end(); ++it ){
      if( it->second <= 0 )
        continue;
      oss << it->first;
      if( it->second > 1 ){
        if( isMarkup )
          oss << "<sub>" << it->second << "</sub>";
        else
          oss << it->second;
      }
    }
    return oss.str();
  }
}//namespace gbioinfo
