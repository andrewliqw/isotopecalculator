#include "validatedentry.hh"

namespace Gbioinfo{

  ValidatedEntry::ValidatedEntry( const Glib::ustring &pattern )
  {
    regex = Glib::Regex::create( pattern );
    Gtk::Entry::signal_changed().connect
      ( sigc::mem_fun( *this, &ValidatedEntry::onChanged ) );
  }

  ValidatedEntry::~ValidatedEntry() {}

  void ValidatedEntry::onChanged()
  {
    Glib::ustring str = get_text();
    if( regex->match( str ) ){
      entryText = str;
    }
    else{
      set_text( entryText );
    }
  }

}//namespace Gbioinfo
