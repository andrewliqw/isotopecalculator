#ifndef GROSSSTICKDIALOG_HH
#define GROSSSTICKDIALOG_HH

#include <gtkmm.h>
#include <validatedentry.hh>

class GrossStickDialog : public Gtk::Dialog
{
public:
  GrossStickDialog( const Glib::ustring &pattern );
  virtual ~GrossStickDialog();

  Glib::ustring getText();

private:
  Gtk::Label label;
  Gbioinfo::ValidatedEntry entry;
};

#endif
