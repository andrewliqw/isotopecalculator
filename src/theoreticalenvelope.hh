#ifndef THEORETICALENVELOPE_HH
#define THEORETICALENVELOPE_HH

#include <vector>
#include <utility>

/** @brief Given the mass and the abundance,
 *  this class calculates the theoretical spectrum.
 *
 *  Except the mass and the abundance, the lower, upper boundary,
 *  step, resolution, normalization parameters are also needed.
 *
 *  The calculation will first compute the left part backward, then
 *  the right part forward until the normalized height are less than
 *  the threshold (0.00001).
 */
class TheoreticalEnvelope{
  friend std::ostream& operator<<( std::ostream &os, const TheoreticalEnvelope & );
public:

  enum ConvolutionFunctionType { Gaussian = 1,
                                 CauchyLorentz = 2,
                                 InverseCauchyLorentz = 3 };

  //@{
  /** @brief the output width for the mass. */
  static int massWidth;
  /** @brief the output precison for the mass. */
  static int massPrecision;
  /** @brief the output width for the abundance. */
  static int abundanceWidth;
  /** @brief the output precision for the abundance. */
  static int abundancePrecision;
  //@}
  //@{
  /** @brief set the output width for the mass. */
  static void setMassWidth( int n );
  /** @brief set the output precison for the mass. */
  static void setMassPrecision( int n );
  /** @brief set the output width for the abundance. */
  static void setAbundanceWidth( int n );
  /** @brief set the output precision for the abundance. */
  static void setAbundancePrecision( int n );
  //@}

  /** @brief define a type of spectrum. */
  typedef std::vector<std::pair<double, double> > SpectrumType;

  /** @brief constructor.
   *
   *  @param lower -- lower boundary
   *  @param upper
   *  @param step
   *  @param r
   *  @param norm
   *  @param type
   *
   *  Note: if upper < lower or step <= 0, a std::runtim_error
   *  will be throwed. Users are needed to deal with this exception.
   */
  TheoreticalEnvelope( double lower, double upper, double step,
                       double r, double norm, ConvolutionFunctionType type );

  void setLower( double );
  void setUpper( double );
  void setStep( double );
  void setResolution( double );
  void setNormalization( double );
  void setFunctionType( ConvolutionFunctionType );

  double getLower();
  double getUpper();
  double getStep();
  double getResolution();
  double getNormalization();
  ConvolutionFunctionType getFunctionType();

  // To do: InverseCauchyLorentz = 3
  void calculateSingleSpecies( double x, double y, bool isLog10 = false );

  /** @brief reset the envelope to zero
   *  according to the lower, upper and step
   */
  void resetEnvelopeZero();

private:

  void gaussian( double, double );
  void cauchyLorentz( double mass, double prob );

  SpectrumType envelope;
  SpectrumType::size_type left, right;
  double lower, upper, step, resolution, normalization;
  ConvolutionFunctionType functionType;

  double threshold;
};


std::ostream& operator<<( std::ostream &os, const TheoreticalEnvelope & );
#endif
