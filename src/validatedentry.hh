#ifndef GBIOINFO_VALIDATEDENTRY_HH
#define GBIOINFO_VALIDATEDENTRY_HH

#include <gtkmm/entry.h>
#include <glibmm/ustring.h>
#include <glibmm/refptr.h>

namespace Gbioinfo{

  class ValidatedEntry : public Gtk::Entry
  {
  public:
    /** @brief constructor. */
    ValidatedEntry
    ( const Glib::ustring &pattern );

    /** @brief destructor. */
    virtual ~ValidatedEntry();

    /** @brief get the text. */
    //Glib::ustring getText();

    /** @brief set the text. */
    //void setText( const Glib::ustring &text );

  private:

    void onChanged();

    Glib::RefPtr<Glib::Regex> regex;
    Glib::ustring entryText;
  };


}//namespace Gbioinfo
#endif
