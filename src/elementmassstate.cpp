#include <sstream>
#include <cmath>
#include <iomanip>

#include "elementmassstate.hh"
#include "globalvar.hh"
#include "atom.hh"

namespace Gbioinfo{
  int ElementMassStates::levelWidth = 10;
  int ElementMassStates::nucleonNumberWidth = 10;
  int ElementMassStates::configurationWidth = 10;
  int ElementMassStates::massWidth = 30;
  int ElementMassStates::massPrecision = 15;
  int ElementMassStates::abundanceWidth = 30;
  int ElementMassStates::abundancePrecision = 15;

  void ElementMassStates::setLevelWidth( int n )
  {
    levelWidth = n;
  }
  void ElementMassStates::setNucleonNumberWidth( int n )
  {
    nucleonNumberWidth = n;
  }
  void ElementMassStates::setConfigurationWidth( int n )
  {
    configurationWidth = n;
  }
  void ElementMassStates::setMassWidth( int n )
  {
    massWidth = n;
  }
  void ElementMassStates::setMassPrecision( int n )
  {
    massPrecision = n;
  }
  void ElementMassStates::setAbundanceWidth( int n )
  {
    abundanceWidth = n;
  }
  void ElementMassStates::setAbundancePrecision( int n )
  {
    abundancePrecision = n;
  }


  int ElementMassStates::getLevelWidth()
  {
    return levelWidth;
  }
  int ElementMassStates::getNucleonNumberWidth()
  {
    return nucleonNumberWidth;
  }
  int ElementMassStates::getConfigurationWidth()
  {
    return configurationWidth;
  }
  int ElementMassStates::getMassWidth()
  {
    return massWidth;
  }
  int ElementMassStates::getMassPrecision()
  {
    return massPrecision;
  }
  int ElementMassStates::getAbundanceWidth()
  {
    return abundanceWidth;
  }
  int ElementMassStates::getAbundancePrecision()
  {
    return abundancePrecision;
  }

  ElementMassStates::ElementMassStates
  ( const std::string &elementSymbol, unsigned int n )
    : elementSymbol( elementSymbol ), atomNum( n )
  {
    //initializeElementAtomPool();
    calculateAllMassStates();
  }

  ElementMassStates::~ElementMassStates()
  {
    for( std::map<std::string, MassState*>::iterator
           it = allMassStates.begin(); it != allMassStates.end(); ++it )
      delete it->second;
  }

  void ElementMassStates::getOneLevelMassStates
  ( unsigned int n, std::vector<MassState*> &ms ) const
  {
    ms.clear();
    for( std::set<const std::string*>::iterator
           it = allLevels[ n ].begin(); it != allLevels[ n ].end(); ++it ){
      ms.push_back( allMassStates.find( **it )->second );
    }
  }

  unsigned int ElementMassStates::getLevelSize() const
  {
    return allLevels.size();
  }

  unsigned int ElementMassStates::getSpeciesSize() const
  {
    return allMassStates.size();
  }

  unsigned int ElementMassStates::getSubLevelSize( unsigned int n ) const
  {
    return allLevels[ n ].size();
  }

  std::string ElementMassStates::getElementSymbol() const
  {
    return elementSymbol;
  }

  unsigned int ElementMassStates::getAtomNumber() const
  {
    return atomNum;
  }

  const ElementMassStates::MassState*
  ElementMassStates::getGroundStatePtr() const
  {
    const std::string* strPtr = *( allLevels[ 0 ].begin() );
    return allMassStates.find( *strPtr )->second;
  }

  const ElementMassStates::MassState&
  ElementMassStates::getGroundState() const
  {
    const std::string* strPtr = *( allLevels[ 0 ].begin() );
    return *( allMassStates.find( *strPtr )->second );
  }

  const ElementMassStates::MassState*
  ElementMassStates::getHighestExcitedStatePtr() const
  {
    const std::string* strPtr = *( allLevels[ allLevels.size() - 1 ].begin() );
    return allMassStates.find( *strPtr )->second;
  }

  const ElementMassStates::MassState&
  ElementMassStates::getHighestExcitedState() const
  {
    const std::string* strPtr = *( allLevels[ allLevels.size() - 1 ].begin() );
    return *( allMassStates.find( *strPtr )->second );
  }

  unsigned int ElementMassStates::getMaxStateLevel() const
  {
    return maxState->getLevel();
  }

  std::string ElementMassStates::genKey( const MassState &ms ) const
  {
    if( ms.configuration.empty() )
      return std::string();

    std::ostringstream ss;
    ss << ms.configuration[ 0 ];

    for( std::vector<unsigned int>::size_type i = 1;
         i < ms.configuration.size(); ++i ){
      ss << '_' << ms.configuration[ i ];
    }

    return ss.str();
  }

  void ElementMassStates::calculateAllMassStates()
  {
    if( Gbioinfo::gbioinfoDebug ){
      std::cerr << "calculate all mass states of element "
                << elementSymbol << std::endl;
    }

    const Element &element = ElementAtomPool::instance()->getElement( elementSymbol );
    unsigned int n = element.getNumberOfIsotopes();
    const Atom &lightestIsotope = element.getIsotope( 0, Element::IsotopeIndex );
    const Atom &heaviestIsotope = element.getIsotope( n - 1, Element::IsotopeIndex );

    // initial all of the levels to empty set
    unsigned int totalLevels =
      ( heaviestIsotope.getMassNumber() - lightestIsotope.getMassNumber() ) * atomNum + 1;
    if( Gbioinfo::gbioinfoDebug ){
      std::cerr << "\ntotalLevels: " << totalLevels << std::endl;
    }
    allLevels.resize( totalLevels );

    // set the ground state in the level 0
    MassState *msPtr = new MassState( this );
    msPtr->configuration = std::vector<unsigned int>( n, 0 );
    msPtr->configuration[ 0 ] = atomNum;
    msPtr->mass = lightestIsotope.mass * atomNum;
    msPtr->log10Prob = log10( lightestIsotope.abundance ) * atomNum;
    msPtr->nucleonNumber = lightestIsotope.getMassNumber() * atomNum;

    std::string str( genKey( *msPtr ) );
    allMassStates[ str ] = msPtr;
    const std::string *strPtr = &( allMassStates.find( str )->first );
    allLevels[ 0 ].insert( strPtr );

    maxState = msPtr;

    // calculate all other mass states
    for( unsigned int level = 0; level != totalLevels - 1; ++level ){
      std::set<const std::string*> &singleLevel = allLevels[ level ];
      for( std::set<const std::string*>::iterator
             it = singleLevel.begin(); it != singleLevel.end(); ++it ){
        MassState &currentMS = *( allMassStates.find( **it )->second );
        std::vector<unsigned int> &configuration = currentMS.configuration;
        for( std::vector<unsigned int>::size_type i = 0;
             i != configuration.size() - 1; ++i )
          if( configuration[ i ] > 0 ){
            std::vector<unsigned int>::size_type j = i + 1;
            --(configuration[ i ]); ++(configuration[ j ]);
            std::string str2( genKey( currentMS ) );
            if( ! allMassStates.count( str2 ) ){
              // can't use j-i because there is gap for isotopes
              const Atom &isotopeI = element.getIsotope( i, Element::IsotopeIndex );
              const Atom &isotopeJ = element.getIsotope( j, Element::IsotopeIndex );
              unsigned int levelStep = isotopeJ.getMassNumber() - isotopeI.getMassNumber();

              msPtr = new MassState( this );
              msPtr->configuration = configuration;
              msPtr->mass = currentMS.mass + isotopeJ.mass - isotopeI.mass;
              msPtr->log10Prob = currentMS.log10Prob +
                log10( configuration[ i ] + 1 ) - log10( configuration[ j ] ) +
                log10( isotopeJ.abundance ) - log10( isotopeI.abundance );
              msPtr->nucleonNumber = currentMS.nucleonNumber + levelStep;

              allMassStates[ str2 ] = msPtr;
              strPtr = &( allMassStates.find( str2 )->first );
              allLevels[ level + levelStep ].insert( strPtr );

              if( msPtr->log10Prob > maxState->log10Prob )
                maxState = msPtr;
            }
            --configuration[ j ]; ++configuration[ i ];
          }
      }
    }

    if( Gbioinfo::gbioinfoDebug ){
      std::cerr << "calculate all mass states of element "
                << elementSymbol << " end" << std::endl;
    }
  }

  ElementMassStates::MassState::MassState( ElementMassStates *ems )
    : ems( ems )
  {}

  unsigned int ElementMassStates::MassState::getLevel() const
  {
    return nucleonNumber - ems->getGroundStatePtr()->nucleonNumber;
  }

  std::ostream& operator<<( std::ostream &os, const ElementMassStates &ems )
  {
    if( Gbioinfo::gbioinfoDebug ){
      std::cerr << "\nEnter operator<<ElementMassStates ..." << std::endl
                << "totalLeveSize: " << ems.getLevelSize() << std::endl;
    }

    for( unsigned int i = 0; i < ems.getLevelSize(); ++i ){
      std::vector<ElementMassStates::MassState*> ms;
      ems.getOneLevelMassStates( i, ms );
      if( ms.empty() )
        continue;

      for( std::vector<ElementMassStates::MassState*>::size_type
             j = 0; j < ms.size(); ++j ){
        os << std::setw( ElementMassStates::getLevelWidth() )
           << ms[ j ]->getLevel()
           << std::setw( ElementMassStates::getNucleonNumberWidth() )
           << ms[ j ]->nucleonNumber;

        if( Gbioinfo::gbioinfoDebug ){
          std::cerr << " configuration: ";
        }

        for( std::vector<unsigned int>::size_type k = 0;
             k < ms[ j ]->configuration.size(); ++k )
          os << std::setw( ElementMassStates::getConfigurationWidth() )
             << ms[ j ]->configuration[ k ];

        if( Gbioinfo::gbioinfoDebug ){
          std::cerr << " mass and abundance: ";
        }
        os << std::setw( ElementMassStates::getMassWidth() )
           << std::setprecision( ElementMassStates::getMassPrecision() )
           << ms[ j ]->mass
           << std::setw( ElementMassStates::getAbundanceWidth() )
           << std::setprecision( ElementMassStates::getAbundancePrecision() )
           << ms[ j ]->log10Prob;
      }
      if( i != ems.getLevelSize() - 1 )
        os << "\n";
    }
    return os;
  }
} //namespce
