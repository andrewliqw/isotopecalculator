#include <iostream>

#include "globalvar.hh"

namespace Gbioinfo
{
  bool gbioinfoWarning = false;
  bool gbioinfoDebug = false;
  RunMode gbioinfoRunMode = GBIOINFO_CML;

  void displayMessage( const Glib::ustring &message,
                       Gtk::MessageType type )
  {
    Glib::ustring typeStr;
    switch( type ){
    case Gtk::MESSAGE_INFO:
      typeStr = "Message(Info): ";
      break;
    case Gtk::MESSAGE_WARNING:
      typeStr = "Message(Warning): ";
      break;
    case Gtk::MESSAGE_QUESTION:
      typeStr = "Message(Question): ";
      break;
    case Gtk::MESSAGE_ERROR:
      typeStr = "Message(Error): ";
      break;
    case Gtk::MESSAGE_OTHER:
      typeStr = "Message(Other): ";
      break;
    }

    Glib::ustring message2 = typeStr + message;

    if( gbioinfoRunMode == GBIOINFO_CML )
      std::cerr << message2 << std::endl;
    else if( gbioinfoRunMode == GBIOINFO_GTKMM ){
      Gtk::MessageDialog dialog( message2, false, type );
      dialog.run();
    }
  }
}//namespace

