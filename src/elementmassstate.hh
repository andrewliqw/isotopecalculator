#ifndef GBIOINFO_ELEMENTMASSSTATE_HH
#define GBIOINFO_ELEMENTMASSSTATE_HH

#include <vector>
#include <string>
#include <map>
#include <set>
#include <iostream>
#include <stdexcept>

namespace Gbioinfo
{
  class ElementMassStateError : public std::runtime_error
  {
  public:
    explicit ElementMassStateError( const std::string &s ) : std::runtime_error( s ) {}
  };

  /** @brief All isotopic mass states of a single element molecule.
   *
   * All isotopic mass states are saved in a map:
   * key is a unique string for each mass state,
   * value is the mass state.
   *
   * The hiarchical structure of all isotopic mass states
   * are saved in a vector.  The element of the vector is
   * a set consists of the unique strings of mass states
   * in the same level.  The index of the element implicitly
   * stands for the level.
   */

  class ElementMassStates{
  public:

    /** @brief auxilary class for ElementMassStates.
     *
     * Each mass state of a molecule has its own characteristic,
     * including configuration, mass, abundance, nucleon number.
     * For convinience, these properties are publicly available.
     * When talking about the LOGIC level, we need the context,
     * i.e., the whole mass states.  Therefore, there is a pointer
     * to the context.
     *
     * The (copy) constructors and the assignment operator are
     * declared as private.  It means you can not make a new object,
     * but you can use the reference or pointer to a object.
     */

    class MassState{
      /** @brief declare class ElementMassStates as friend to
       * create a MassState object.
       */
      friend class ElementMassStates;

    public:

      /** @brief get the level (it is the nucleon number difference
       * between current state and the ground state, indexed from zero). */
      unsigned int getLevel() const;

      /** @brief configuration number of current mass state. */
      std::vector<unsigned int> configuration;

      /** @brief mass of current mass state. */
      double mass;

      /** @brief logrithm of abundance of current mass state. */
      double log10Prob;

      /** @brief nucleon number of current mass state. */
      unsigned int nucleonNumber;

    private:

      /** @brief Constructor. */
      MassState( ElementMassStates *ems );

      /** @brief copy constructor. */
      MassState( const MassState & );

      /** @brief assignment operator */
      MassState& operator=( const MassState & );

      /** @brief pointer to ElementMassStates. */
      ElementMassStates *ems;
    };

    /** @brief Constructor.
     *
     * @param elementSymbol -- the element's symbol.
     * @param n -- the number of atoms of that Element object.
     */
    ElementMassStates( const std::string &elementSymbol, unsigned int n );

    /** @brief Destructor. */
    ~ElementMassStates();

    /** @brief get a pointer to the ground state. */
    const MassState* getGroundStatePtr() const;
    /** @brief get the ground state. */
    const MassState& getGroundState() const;
    /** @brief get a pointer to the highest excited state. */
    const MassState* getHighestExcitedStatePtr() const;
    /** @brief get the highest excited state. */
    const MassState& getHighestExcitedState() const;

    /** @brief get the level of the maximum mass state (index from zero). */
    unsigned int getMaxStateLevel() const;

    /** @brief get a reference to the map which saves all mass states. */
    const std::map<std::string, MassState*>& getAllMassStates() const;

    /** @brief get all mass states in one level. */
    void getOneLevelMassStates
    ( unsigned int n, std::vector<MassState*>& ) const;

    /** @brief get the size of level. */
    unsigned int getLevelSize() const;
    /** @brief get the size of all species. */
    unsigned int getSpeciesSize() const;

    /** @brief get the size of a sub level. */
    unsigned int getSubLevelSize( unsigned int n ) const;

    /** @brief get the element symbol. */
    std::string getElementSymbol() const;
    /** @brief get the number of atoms. */
    unsigned int getAtomNumber() const;

    /** @brief generate a unique string for a mass state. */
    std::string genKey( const MassState &massState ) const;

  private:

    /** @brief calculate all of mass states. */
    void calculateAllMassStates();

    /** @brief element information. */
    std::string elementSymbol;

    /** @brief number of atoms. */
    unsigned int atomNum;

    /** @brief all mass states saved in a map.
     *
     * key is a unique string standing for a specific mass state,
     * value is a pointer to that mass state.
     * @sa genKey.
     */
    std::map<std::string, MassState*> allMassStates;

    /** @brief hierarchical structure information.
     *
     * 
     */
    std::vector<std::set<const std::string*> > allLevels;

    /** @brief maximum mass state. */
    const MassState *maxState;

    // The following is for the output.

  public:

    //@{
    /** @brief set the output width for the level. */
    static void setLevelWidth( int n );
    /** @brief set the output width for the nucleon number. */
    static void setNucleonNumberWidth( int n );
    /** @brief set the output width for the configuration. */
    static void setConfigurationWidth( int n );
    /** @brief set the output width for the mass. */
    static void setMassWidth( int n );
    /** @brief set the output precison for the mass. */
    static void setMassPrecision( int n );
    /** @brief set the output width for the abundance. */
    static void setAbundanceWidth( int n );
    /** @brief set the output precision for the abundance. */
    static void setAbundancePrecision( int n );
    //@}

    //@{
    /** @brief get the output width for the level. */
    static int getLevelWidth();
    /** @brief get the output width for the nucleon number. */
    static int getNucleonNumberWidth();
    /** @brief get the output width for the configuration. */
    static int getConfigurationWidth();
    /** @brief get the output width for the mass. */
    static int getMassWidth();
    /** @brief get the output precison for the mass. */
    static int getMassPrecision();
    /** @brief get the output width for the abundance. */
    static int getAbundanceWidth();
    /** @brief get the output precision for the abundance. */
    static int getAbundancePrecision();
    //@}

  private:
    //@{
    /** @brief the output width for the level. */
    static int levelWidth;
    /** @brief the output width for the nucleon number. */
    static int nucleonNumberWidth;
    /** @brief the output width for the configuration. */
    static int configurationWidth;
    /** @brief the output width for the mass. */
    static int massWidth;
    /** @brief the output precison for the mass. */
    static int massPrecision;
    /** @brief the output width for the abundance. */
    static int abundanceWidth;
    /** @brief the output precision for the abundance. */
    static int abundancePrecision;
    //@}
  };
 
  std::ostream& operator<<( std::ostream &os, const ElementMassStates &ems );
}
#endif
