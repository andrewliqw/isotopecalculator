

#include "grossstickdialog.hh"

GrossStickDialog::GrossStickDialog( const Glib::ustring &pattern )
  : label( "Please input probability threshold" ), entry( pattern )
{
  set_title( "Please input probability threshold" );
  Gtk::VBox *box = get_vbox();
  box->pack_start( label );
  box->pack_start( entry );

  add_button( Gtk::Stock::OK, Gtk::RESPONSE_OK );
  add_button( Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );

  entry.set_text( "0.99" );

  show_all_children();
}

GrossStickDialog::~GrossStickDialog()
{}

Glib::ustring GrossStickDialog::getText()
{
  return entry.get_text();
}

#ifdef TEST_CODE
int main( int argc, char *argv[] )
{
  Gtk::Main kit( argc, argv );

  GrossStickDialog dialog( "(^0?\\.\\d*$|^[01]?$" );
  int result = dialog.run();
  switch( result ){
  case Gtk::RESPONSE_OK:
    break;
  case Gtk::RESPONSE_CANCEL:
    break;
  }
}
#endif
