#ifndef COMPOUNDMASSSTATES_HH
#define COMPOUNDMASSSTATES_HH

#include <deque>
#include <set>
#include <fstream>
#include <elementmassstate.hh>

#include "theoreticalenvelope.hh"

/** @brief class CompoundMassStates
 *
 *  This class is used to calculate the fine structures
 * of the molecule given the fine structures of the
 * elements.
 */

class CompoundMassStates{

public:

  /** @brief enumrate for controling average and/or output
   *
   * This enumerate controls what should be done when
   * nevigate a given level.
   */
  typedef unsigned int NavigateLevelAction;

  //const static NavigateLevelAction OUTPUT = 1;
  const static NavigateLevelAction EACHSTATEOUTPUT = 2;
  const static NavigateLevelAction LEVELSTATENUM = 4;
  const static NavigateLevelAction LEVELTOTALABUNDANCE = 8;
  const static NavigateLevelAction LEVELAVERAGEMASS = 10;
  const static NavigateLevelAction ENVELOPE = 16;

  CompoundMassStates
  ( const std::string &chemicalFormula );

  ~CompoundMassStates();

  /** @brief set the charge state.
   *
   * The final output will be the m/z.
   */
  void setCharge( int c );
  int getCharge() const;

  void outputGlobalInformation( std::ostream &, bool ) const;

  /** @brief set current molecule mass state to ground state. */
  void setCurrentMoleculeMassState2GroundState() const;

  /** @brief set current molecule mass state to highest excited state. */
  void setCurrentMoleculeMassState2HighestExcitedState() const;

  /** @brief get the level of highest excited state. */
  unsigned int getHighestExcitedStateLevel() const;
  /** @brief get the level of maximum state. */
  unsigned int getMaximumStateLevel() const;

  /** @brief calculate the gross stick. */
  void calculateGrossStick
  ( std::deque<std::pair<unsigned int, double> > &, double threshold ) const;
  /** @brief calculate isotopic species number of each level. */
  void calculateSpeciesNumberOfGrossStructure( std::vector<double> &number );



  /** @brief set the output stream. */
  void setOutputStream( std::ostream *os );
  bool setOutputStream( std::string filename );

  void setEnvelopePtr( TheoreticalEnvelope *te );

  /** @brief output all of the mass states within a given level
   *
   * @param level which level is printed
   * @param action what kind of action when navigating the level
   */
  void navigateOneLevel( unsigned int level,
			 NavigateLevelAction action ) const;


  /** @brief get the formula of the molecule. */
  const std::string getChemicalFormula() const;


  //@{
  /** @brief global information. */
  unsigned int minNucleonNumber;
  unsigned int maxNucleonNumber;
  unsigned int maxLog10ProbLevel;
  unsigned int totalLevel;
  double totalSpeciesNum;
  double moleculeAverageMass;
  //@}

  //@{
  /** @brief current molecule mass state information. */
  //mutable unsigned int currentMoleculeMassStateLevel;
  mutable unsigned int currentMoleculeMassStateNucleonNumber;
  mutable std::vector<unsigned int> currentMoleculeMassStateConfiguration;
  mutable double currentMoleculeMassStateMass;
  mutable double currentMoleculeMassStateLog10Prob;
  //@}

private:

  /** @brief update meta information after append a new element
   *
   * Function update will recalculate some meta information
   * e.g., minimum/maximum nuclon number, total number of mass states.
   */
  void update();

  //@{
  /** @brief print all of the mass states within the given level
   *
   * @param level
   * @param action
   * @param elementIndex
   */
  void navigateOneLevelRecursive
  ( unsigned int level, unsigned int elementIndex ) const;

  /** @brief print all of the mass states within a specific combination
   * of a given level
   *
   * @param egsIndex
   * @param action
   * @param elementIndex
   */
  void navigateSubLevelRecursive ( unsigned int elementIndex ) const;

  /** @brief print the detail information of a given molecule mass state
   *
   * @param os
   */
  void outputCurrentMoleculeMassState() const;

  //@}

  //@{
  //void calculateCurrentMoleculeMassStateLevel();
  void calculateCurrentMoleculeMassStateNucleonNumber() const;
  void calculateCurrentMoleculeMassStateConfiguration() const;
  void calculateCurrentMoleculeMassStateMass() const;
  void calculateCurrentMoleculeMassStateLog10Prob() const;
  //@}

  /** @brief auxiliary vector for recursive calculation.
   *
   * This auxiliary vector is used to keep the possible maximum
   * level from current element to the last element.  So
   * it is convinient to determine the minimum
   * level for current element in the recursive calculation,
   * also it avoids repeat calculation.
   */
  std::vector<unsigned int> maxTable;

  /** @brief charge keeps the charge state.
   *
   * charge keeps the charge state.
   */
  int charge;

  /** @brief composition keeps the fine structures for each element.
   *
   */
  std::vector<const Gbioinfo::ElementMassStates*> composition;


  //@{
  /** @brief current level information. */
  mutable unsigned int currentLevel;
  mutable double currentLevelStateNumber;
  mutable double currentLevelTotalAbundance;
  mutable double currentLevelAverageMass;
  mutable NavigateLevelAction currentAction;
  //@}

  /** @brief keep the each element level index. */
  mutable std::vector<unsigned int> currentSubLevel;

  /** @brief the information of the current molecule mass state
   *
   * the current molecule mass state consists of mass states from each
   * element.
   */
  mutable std::vector<const Gbioinfo::ElementMassStates::MassState*>
  currentMoleculeMassState;

  mutable std::ostream *os;
  TheoreticalEnvelope *te;
};

#endif
