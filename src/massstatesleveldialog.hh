#ifndef MASSSTATESLEVELDIALOG_HH
#define MASSSTATESLEVELDIALOG_HH

#include <gtkmm.h>

class MassStatesLevelDialog : public Gtk::Dialog
{
public:
  MassStatesLevelDialog();
  virtual ~MassStatesLevelDialog();

  double getLeftValue() const;
  double getRightValue() const;

  void setLeftRange( double minValue, double maxValue );
  void setRightRange( double minValue, double maxValue );

private:

  void setRightLevelMin();

  Gtk::Label leftLabel, rightLabel;
  Gtk::Adjustment leftAdjustment, rightAdjustment;
  Gtk::SpinButton leftLevel, rightLevel;
  Gtk::VBox leftBox, rightBox;
  Gtk::HBox hbox;

};

#endif
