#ifndef ENVELOPEDIALOG_HH
#define ENVELOPEDIALOG_HH

#include <gtkmm.h>
#include <validatedentry.hh>

#include "theoreticalenvelope.hh"

class EnvelopeDialog : public Gtk::Dialog
{
public:
  EnvelopeDialog();
  virtual ~EnvelopeDialog();

  unsigned int getLeftLevel();
  unsigned int getRightLevel();
  double getLower();
  double getUpper();
  double getStep();
  double getResolution();
  double getNormalization();

  TheoreticalEnvelope::ConvolutionFunctionType getType();

  bool setMaxLevel( double );

  bool hasBlankEntry();

private:
  void setRightLevelMin();

  Gtk::Label leftLabel, rightLabel;
  Gtk::Adjustment leftAdjustment, rightAdjustment;
  Gtk::SpinButton leftLevel, rightLevel;
  Gtk::VBox leftBox, rightBox;
  Gtk::HBox hbox, hbox2, hbox3;

  Gtk::Label lowerLabel, upperLabel, stepLabel, rLabel, normLabel;
  Gbioinfo::ValidatedEntry lowerEntry, upperEntry, stepEntry, rEntry, normEntry;

  Gtk::HBox buttonBox;
  Gtk::RadioButton gaussianButton, cauchyLorentzButton, inverseCauchyLorentzButton;

  // There internal variables are used to remember the current status
  //double leftLevelLast, rightLevelLast;
  //Glib::ustring lowerLast, upperLast, stepLast, rLast, normLast;
  //TheoreticalEnvelope::ConvolutionFunctionType functionTypeLast;
};


#endif
