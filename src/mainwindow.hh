#include <gtkmm.h>

#include "compoundmassstates.hh"

class IsotopeColumns : public Gtk::TreeModel::ColumnRecord
{
public:

  IsotopeColumns()
  {
    add( atomicNumber );
    add( elementName );
    add( massNumber );
    add( mass );
    add( abundance );
  }
    
  Gtk::TreeModelColumn<unsigned int> atomicNumber;
  Gtk::TreeModelColumn<Glib::ustring> elementName;
  Gtk::TreeModelColumn<unsigned int> massNumber;
  Gtk::TreeModelColumn<double> mass;
  Gtk::TreeModelColumn<double> abundance;
};

class MainWindow : public Gtk::Window
{
public:
  MainWindow();
  virtual ~MainWindow();

private:

  void onActionFileLoadIsotopeFile();
  void onActionFileExportGrossStick();
  void onActionFileExportGrossSpeciesNumber();
  void onActionFileExportFineStructures();
  void onActionFileExportEnvelope();
  void onActionFileQuit();

  void onActionCalculateGrossStick();
  void onActionCalculateGrossSpeciesNumber();
  void onActionCalculateFineStuctures();
  void onActionCalculateEnvelope();

  void onActionHelpAbout();

  void setIsotopeColumnsData( const std::string &filename = "" );

  void onChemicalFormulaEntryActivate();
  void onChargeSpinButtonValueChanged();
  void onClearButtonClicked();

  //void clearSummaryLabel();

  Gtk::VBox vBox;
  Glib::RefPtr<Gtk::UIManager> uiManager;
  Glib::RefPtr<Gtk::ActionGroup> actionGroup;
  Glib::RefPtr<Gtk::Action> actionFileLoadIsotope;
  Glib::RefPtr<Gtk::Action> actionFileExportGrossStick;
  Glib::RefPtr<Gtk::Action> actionFileExportGrossSpeciesNumber;
  Glib::RefPtr<Gtk::Action> actionFileExportFineStructures;
  Glib::RefPtr<Gtk::Action> actionFileExportEnvelope;
  Glib::RefPtr<Gtk::Action> actionCalculateGrossStick;
  Glib::RefPtr<Gtk::Action> actionCalculateGrossSpeciesNumber;
  Glib::RefPtr<Gtk::Action> actionCalculateEnvelope;
  Glib::RefPtr<Gtk::Action> actionHelpAbout;
  // input formula, charge state
  Gtk::HBox hBox, hBox2;
  Gtk::Entry chemicalFormulaEntry;
  Gtk::Button clearButton;
  Gtk::Label chargeLabel;
  Gtk::Adjustment chargeAdjustment;
  Gtk::SpinButton chargeSpinButton;

  Gtk::Label summaryLabel;

  // isotope information
  Gtk::ScrolledWindow scrolledWindow;
  Gtk::TreeView isotopeView;
  IsotopeColumns isotopeColumns;
  Glib::RefPtr<Gtk::ListStore> isotopeColumnsRef;

  CompoundMassStates *cms;
  std::deque<std::pair<unsigned int, double> > grossStick;
  double grossStickThreshold;
  std::vector<double> grossSpeciesNumber;
  TheoreticalEnvelope *te;
  unsigned int leftLevel, rightLevel;
};
