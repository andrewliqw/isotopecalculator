#include "massstatesleveldialog.hh"

MassStatesLevelDialog::MassStatesLevelDialog()
  : leftLabel( "Left:" ), rightLabel( "Right:" ),
    leftAdjustment( 0, 0, 10, 1, 1, 0 ),
    rightAdjustment( 0, 0, 10, 1, 1, 0 ),
    leftLevel( leftAdjustment ),
    rightLevel( rightAdjustment )
{
  set_title( "Set the level" );
  Gtk::VBox *box = get_vbox();

  //Gtk::VBox leftBox;
  leftBox.pack_start( leftLabel );
  leftBox.pack_start( leftLevel );

  //Gtk::VBox rightBox;
  rightBox.pack_start( rightLabel );
  rightBox.pack_start( rightLevel );

  //Gtk::HBox hbox;
  hbox.pack_start( leftBox );
  hbox.pack_start( rightBox );

  box->pack_start( hbox );
  //Gtk::VBox box;
  //box.pack_start( hbox );

  add_button( Gtk::Stock::OK, Gtk::RESPONSE_OK );
  add_button( Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );

  leftLevel.signal_value_changed().connect
    ( sigc::mem_fun( *this, &MassStatesLevelDialog::setRightLevelMin ) );

  show_all_children();
}

MassStatesLevelDialog::~MassStatesLevelDialog()
{}

void MassStatesLevelDialog::setLeftRange
(double minValue, double maxValue )
{
  leftLevel.set_range( minValue, maxValue );
}

void MassStatesLevelDialog::setRightRange
(double minValue, double maxValue )
{
  rightLevel.set_range( minValue, maxValue );
}

void MassStatesLevelDialog::setRightLevelMin()
{
  setRightRange( leftLevel.get_value(), leftAdjustment.get_upper() );
}

double MassStatesLevelDialog::getLeftValue() const
{
  return leftLevel.get_value();
}

double MassStatesLevelDialog::getRightValue() const
{
  return rightLevel.get_value();
}
