//#include <boost/program_options.hpp>
#include <globalvar.hh>

#include "mainwindow.hh"


int main( int argc, char *argv[] )
{
  Gtk::Main kit( argc, argv );

  /*
  namespace boostpo = boost::program_options;
  boostpo::options_description cmlOptions( "Allowed options" );
  cmlOptions.add_options()
    ( "help", "produce help message" )
    ( "command-line-mode", "run this program in command line mode" )
    //( "fasta-file", boostpo::value<std::string>( &fastaFilename ), "fasta filename" )
    //( "atom-file", boostpo::value<std::string>( &atomFilename ), "atom filename" );
    ( "gbioinfo-debug", "run this program in debug mode" );

  boostpo::variables_map cmlVm;
  boostpo::store( boostpo::command_line_parser( argc, argv ).
                  options( cmlOptions ).run(), cmlVm );
  boostpo::notify( cmlVm );

  if( cmlVm.count( "gbioinfo-debug" ) )
    Gbioinfo::gbioinfoDebug = true;
  */

  //Gbioinfo::gbioinfoDebug = true;
  Gbioinfo::gbioinfoRunMode = Gbioinfo::GBIOINFO_GTKMM;
  MainWindow window;
  Gtk::Main::run( window );

  return 0;
}
