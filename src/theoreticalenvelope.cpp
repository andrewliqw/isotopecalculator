#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <utility>
#include <cmath>
#include <globalvar.hh>

#include "theoreticalenvelope.hh"

int TheoreticalEnvelope::massWidth = 30;
int TheoreticalEnvelope::massPrecision = 15;
int TheoreticalEnvelope::abundanceWidth = 30;
int TheoreticalEnvelope::abundancePrecision = 15;

TheoreticalEnvelope::TheoreticalEnvelope
( double lower, double upper, double step,
  double r, double norm, ConvolutionFunctionType type )
  :
  lower( lower ), upper( upper ), step( step ),
  resolution( r ), normalization( norm ), functionType( type ),
  threshold( 0.00000001 )
{
  if( lower <= 0 ){
    std::string message( "TheoreticalEnvelope: lower <= 0" );
    Gbioinfo::displayMessage( message, Gtk::MESSAGE_ERROR );
    throw std::runtime_error( message );
  }
  if( upper < lower ){
    std::string message( "TheoreticalEnvelope: upper < lower" );
    Gbioinfo::displayMessage( message, Gtk::MESSAGE_ERROR );
    throw std::runtime_error( message );
  }
  if( step <= 0 ){
    std::string message( "TheoreticalEnvelope: step <= 0" );
    Gbioinfo::displayMessage( message, Gtk::MESSAGE_ERROR );
    throw std::runtime_error( message );
  }
  unsigned int n = static_cast<unsigned int>( ( upper - lower ) / step ) + 1;
  envelope.resize( n );
  envelope[ 0 ].first = lower;
  envelope[ 0 ].second = 0.;
  for( SpectrumType::size_type i = 1; i < envelope.size(); ++i ){
    envelope[ i ].first = envelope[ i - 1 ].first + step;
    envelope[ i ].second = 0.;
  }

  left = 0;
  right = 0;
}

void TheoreticalEnvelope::setLower( double x )
{
  lower = x;
}

void TheoreticalEnvelope::setUpper( double x )
{
  upper = x;
}

void TheoreticalEnvelope::setStep( double x )
{
  step = x;
}

void TheoreticalEnvelope::setResolution( double x )
{
  resolution = x;
}

void TheoreticalEnvelope::setNormalization( double x )
{
  normalization = x;
}

void TheoreticalEnvelope::setFunctionType( ConvolutionFunctionType type )
{
  functionType = type;
}

double TheoreticalEnvelope::getLower()
{
  return lower;
}

double TheoreticalEnvelope::getUpper()
{
  return upper;
}

double TheoreticalEnvelope::getStep()
{
  return step;
}

double TheoreticalEnvelope::getResolution()
{
  return resolution;
}

double TheoreticalEnvelope::getNormalization()
{
  return normalization;
}

TheoreticalEnvelope::ConvolutionFunctionType TheoreticalEnvelope::getFunctionType()
{
  return functionType;
}

void TheoreticalEnvelope::resetEnvelopeZero()
{
  unsigned int n = static_cast<unsigned int>( ( upper - lower ) / step ) + 1;
  envelope.resize( n );
  envelope[ 0 ].first = lower;
  envelope[ 0 ].second = 0.;
  for( SpectrumType::size_type i = 1; i < envelope.size(); ++i ){
    envelope[ i ].first = envelope[ i - 1 ].first + step;
    envelope[ i ].second = 0.;
  }
}

void TheoreticalEnvelope::calculateSingleSpecies
( double mass, double prob, bool isLog10 )
{
  if( isLog10 )
    prob = std::pow( 10., prob );

  switch( functionType ){
  case Gaussian:
    gaussian( mass, prob );
    break;
  case CauchyLorentz:
    cauchyLorentz( mass, prob );
    break;
  case InverseCauchyLorentz:
    ;
    break;
  }
}

/*
void TheoreticalEnvelope::gaussian( double mass, double prob )
{
  // sqrt( log( 256. ) ) = 2.354820045030949
  double sigma = mass / ( 2.354820045030949 * resolution );
  double twoSigmaSquare = 2. * sigma * sigma;
  double fiveSigma = 5. * sigma;
  while( envelope[ left ].first < mass - fiveSigma && left < envelope.size() - 1 )
    ++left;
  while( envelope[ left ].first > mass - fiveSigma && left >= 1 )
    --left;
  right = left;
  while( envelope[ right ].first < mass + fiveSigma && right < envelope.size() - 1 )
    ++right;

  for( SpectrumType::size_type i = left; i <= right; ++i ){
    double massDiff = envelope[ i ].first - mass;
    envelope[ i ].second += normalization * prob *
      exp( - massDiff * massDiff / twoSigmaSquare );
  }
}
*/

void TheoreticalEnvelope::gaussian( double mass, double prob )
{
  // sqrt( log( 256. ) ) = 2.354820045030949
  double sigma = mass / ( 2.354820045030949 * resolution );
  double twoSigmaSquare = 2. * sigma * sigma;
  /*
  // sqrt(2*pi) = 2.506628274631000
  double A = 1./ 2.506628274631000 / sigma;
  */
  double A = 1.;
  
  while( envelope[ left ].first < mass && left < envelope.size() - 1 )
    ++left;
  while( envelope[ left ].first > mass && left >= 1 )
    --left;
  right = left + 1;

  SpectrumType::size_type i = left;
  // left part, note the special case of i==0 (--0!!!)
  do{
    double massDiff = envelope[ i ].first - mass;
    double y = normalization * prob * A *
      exp( - massDiff * massDiff / twoSigmaSquare );
    envelope[ i ].second += y;
    if( y < threshold )
      break;
    --i;
  }while( i > 0 );
  if( i == 0 ){
    double massDiff = envelope[ i ].first - mass;
    double y = normalization * prob * A *
      exp( - massDiff * massDiff / twoSigmaSquare );
    envelope[ i ].second += y;
  }

  // right part
  for( i = right; i < envelope.size(); ++i ){
    double massDiff = envelope[ i ].first - mass;
    double y =  normalization * prob * A *
      exp( - massDiff * massDiff / twoSigmaSquare );
    envelope[ i ].second += y;
    if( y < threshold )
      break;
  }
}

void TheoreticalEnvelope::cauchyLorentz( double mass, double prob )
{
  double rSquare = resolution * resolution;
  while( envelope[ left ].first < mass && left < envelope.size() - 1 )
    ++left;
  while( envelope[ left ].first > mass && left >= 1 )
    --left;
  right = left + 1;

  // left part, note the special case of i==0 (--0!!!)
  SpectrumType::size_type i = left;
  do{
    double massDiff = envelope[ i ].first - mass;
    double massSquare = mass * mass;
    double y = normalization * prob * massSquare /
      ( massSquare + 4. * rSquare * massDiff * massDiff );
    envelope[ i ].second += y;
    if( y < threshold )
      break;
    --i;
  }while( i > 0 );
  if( i == 0 ){
    double massDiff = envelope[ i ].first - mass;
    double massSquare = mass * mass;
    double y = normalization * prob * massSquare /
      ( massSquare + 4. * rSquare * massDiff * massDiff );
    envelope[ i ].second += y;
  }

  // right part
  for( i = right; i < envelope.size(); ++i ){
    double massDiff = envelope[ i ].first - mass;
    double massSquare = mass * mass;
    double y = normalization * prob * massSquare /
      ( massSquare + 4. * rSquare * massDiff * massDiff );
    envelope[ i ].second += y;
    if( y < threshold )
      break;
  }
}

std::ostream& operator<<( std::ostream &os, const TheoreticalEnvelope &envelope )
{
  if( envelope.envelope.empty() )
    return os;

  os << std::fixed
     << std::setw( TheoreticalEnvelope::massWidth )
     << std::setprecision( TheoreticalEnvelope::massPrecision )
     << envelope.envelope[ 0 ].first
     << std::setw( TheoreticalEnvelope::abundanceWidth )
     << std::setprecision( TheoreticalEnvelope::abundancePrecision )
     << envelope.envelope[ 0 ].second;
  for( TheoreticalEnvelope::SpectrumType::size_type i = 1;
       i < envelope.envelope.size(); ++i )
    os << std::endl
       << std::setw( TheoreticalEnvelope::massWidth )
       << std::setprecision( TheoreticalEnvelope::massPrecision )
       << envelope.envelope[ i ].first
       << std::setw( TheoreticalEnvelope::abundanceWidth )
       << std::setprecision( TheoreticalEnvelope::abundancePrecision )
       << envelope.envelope[ i ].second;
  return os;
}
