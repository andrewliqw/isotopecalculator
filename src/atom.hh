#ifndef GBIOINFO_ATOM_HH
#define GBIOINFO_ATOM_HH

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <stdexcept>

#include "globalvar.hh"

namespace Gbioinfo
{
  class ElementAtomError : public std::runtime_error
  {
  public:
    explicit ElementAtomError( const std::string& s ) :
      std::runtime_error( s ) {}
  };

  class Atom
  {
    friend class ElementAtomPool;

  public:
    unsigned int getMassNumber() const
    {
      return protonNumber + neutronNumber;
    }

    // these data member could not be changed, because the const object
    // return from the Element class
    unsigned int protonNumber;
    unsigned int neutronNumber;
    double mass;
    double abundance;

  private:
    Atom( unsigned int m, unsigned int n, double x, double y ) :
      protonNumber( m ), neutronNumber( n ), mass( x ), abundance( y ) {}
    Atom( const Atom& );
    Atom& operator=( const Atom& );
  };

  class Element
  {
    friend class ElementAtomPool;

  public:
    enum IndexOrMassNumber {IsotopeIndex, IsotopeMassNumber};

    unsigned int getNumberOfIsotopes() const;
    const Atom &getIsotope( unsigned int n, IndexOrMassNumber nType ) const;

    double getMonoMass() const;
    double getAverageMass() const;

    unsigned int atomicNumber;
    std::string symbol;
    std::string name;

  private:
    Element( unsigned int n, const std::string& str, const std::string& str2 ) :
      atomicNumber( n ), symbol( str ), name( str2 ) {}
    Element( const Element& );
    Element& operator=( const Element& );
  };


  class ElementAtomPool
  {
    friend class Element;
    friend std::ostream& operator<<( std::ostream &os, const ElementAtomPool &eap );
  public:
    static ElementAtomPool* instance()
    {
      if( !pool ){
        pool = new ElementAtomPool();
	pool->initializeElements();
	pool->loadAtoms();
      }
      return pool;
    }

    void loadAtoms();
    void loadAtoms( const std::string& filename );

    void getAllAtomicNumber( std::vector<unsigned int> &vec );
    void getAllElementSymbol( std::vector<std::string> &vec );

    double getAtomMass( const std::string &elementSymbol, const unsigned int massNumber );
    double getMonoMass( const std::string &formula );
    double getAverageMass( const std::string &formula );
    double getMonoMass( const std::map<std::string, unsigned int> &formula );
    double getAverageMass( const std::map<std::string, unsigned int> &formula );

    const Element& getElement( const std::string& elementSymbol )
    {
      if( Gbioinfo::gbioinfoDebug ){
	std::cerr << "\nEnter Element::getElement ... " << std::endl;
      }
      std::map<std::string, Element*>::const_iterator it =
        elementsBySymbol.find( elementSymbol );
      if( it == elementsBySymbol.end() )
        throw ElementAtomError( "No such element: " + elementSymbol );
      return *(it->second);
    }

    const Element& getElement( const unsigned int n )
    {
      if( Gbioinfo::gbioinfoDebug ){
	std::cerr << "\nEnter Element::getElement ... " << std::endl;
      }
      std::map<unsigned int, Element*>::const_iterator it =
        elementsByAtomicNumber.find( n );
      if( it == elementsByAtomicNumber.end() ){
	std::stringstream ss( "No such element: its atomic number is ");
	ss << n;
        throw ElementAtomError( ss.str() );
      }
      return *(it->second);
    }

    ~ElementAtomPool();

  private:
    ElementAtomPool() {};
    ElementAtomPool( const ElementAtomPool& );
    ElementAtomPool& operator=( const ElementAtomPool& );

    void initializeElements();
    void calculateIsotope();
    void calculateMonoAverageMass();

    std::set<Element*> elements;
    std::set<Atom*> atoms;

    std::map<std::string, Element*> elementsBySymbol;
    std::map<unsigned int, Element*> elementsByAtomicNumber;
    std::map<std::string, std::pair<double, double> > monoAverageMass;

    // element, mass number and atom
    std::map<std::string, std::map<unsigned int, Atom*> > isotopes;
    // element, all mass numbers
    std::map<std::string, std::vector<unsigned int> > isotopeMassNumber;

    static ElementAtomPool *pool;
  };

  std::ostream& operator<<( std::ostream &os, const ElementAtomPool &eap );

  class ChemicalFormula
  {
  public:
    static std::map<std::string, unsigned int> string2map( const std::string& );
    static std::string map2string( const std::map<std::string, unsigned int> &, bool );
  };

}//namespace

#endif
